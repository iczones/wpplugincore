# Bridge classes
Bridge classes are wrappers around wordpress functionalities and other plugins to simplify and centralize usage.

The classes are not services. They only contain static methods.

There are currently 3 classes:

* [Wordpress](../src/Bridge/Wordpress.php): Wrapper for any wordpress globals, constants and functions
* [Wpml](../src/Bridge/Wpml.php): Wrapper for WPML plugin state, globals, filters, etc
* [Yoast](../src/Bridge/Yoast.php): Wrapper for Yoast plugin state, globals, filters, etc