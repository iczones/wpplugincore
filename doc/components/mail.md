# Mail component
The mail component wraps the wordpress mailing system into a simple to use OOP implementation.

## Configuration
You can configure the component with the [MailConfig](../../src/Components/Mail/MailConfig.php) service.

| Method            | Effect                                                                      |
| :---------------- | :-------------------------------------------------------------------------: |
| setAlwaysBcc      | Adds these BCC on every email sent with the mailer service                  |
| setAlwaysCc       | Adds these CC on every email sent with the mailer service                   |
| setDefaultFrom    | Sets the default FROM header on every mail sent with the mailer service     |
| setDefaultReplyTo | Sets the default REPLY-TO header on every mail sent with the mailer service |

## Usage


Configuring in the plugin file
```php
use ICZones\WPCore\Components\Mail\MailConfig;

MailConfig::getInstance()
    ->setAlwaysBcc(['bcc@example.com'])
    ->setAlwaysCc(['cc@example.com'])
    ->setDefaultFrom('no-reply@example.com')
    ->setDefaultReplyTo('support@example.com');
```

Most basic example
```php
use ICZones\WPCore\Components\Mail\Mailer;
use ICZones\WPCore\Components\Mail\Message\PlainTextMessage;

$message = new PlainTextMessage('A message subject', 'This is the content!');

Mailer::getInstance()->sendMail('destination@address.com', $message);
```

There are other message classes available like [HtmlMessage](../../src/Components/Mail/Message/HtmlMessage.php) 
and [HtmlTemplateMessage](../../src/Components/Mail/Message/HtmlTemplateMessage.php).

The `HtmlTemplateMessage` requires you to configure the [MVC component](./mvc.md) specifically the renderer service.

### Custom messages
You can create custom message implementation by implementing the [MessageInterface](../../src/Components/Mail/Message/MessageInterface.php).

```php
use ICZones\WPCore\Components\Mail\Message\MessageInterface;

class MyCustomMessage implements MessageInterface
{
    public function getSubject() : string
    {
        return 'This is my custom subject';
    }
    
    public function getContent() : string
    {
        return 'This is my custom content';
    }
    
    public function getHeaders() : array
    {
        return array();
    }
    
    public function getAttachments() : array
    {
        // You can use the MessageAttachmentTrait for quick attachment management implementation
        return array();
    }
}
```