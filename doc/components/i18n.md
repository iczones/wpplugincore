# I18n component
The i18n component is used for translating strings and managing locales.

## Configuration
You can configure the component with the [I18nConfig](../../src/Components/I18n/I18nConfig.php) service.

| Method                | Effect                                                                                                                                                           |
| :-------------------- | :--------------------------------------------------------------------------------------------------------------------------------------------------------------: |
| setAvailableLocales   | Sets the available translation locales. Consider making an enum to contain them.                                                                                 |
| setGettextResources   | Sets gettext domain and resource directory from which gettext will get its PO/MO files.                                                                          |
| addTranslationCatalog | Adds a translation catalog to the translator and at the same time enables the translator. Contrary to gettext resource, a catalog is used within the translator. |

## Usage

In translations/translations.fr.php
```php
return [
    'hello_word' => 'Bonjour le monde',
    'hello_world_gettext' => __('my_domain', 'Hello world'),
    'nested' => [
        'example' => 'Exemple imbriqué'        
    ]
];
```

Configuring in the plugin file
```php
use ICZones\WPCore\Components\I18n\I18nConfig;
use ICZones\WPCore\Components\I18n\Translator\Catalogs\FileCatalog;

I18nConfig::getInstance()
    ->setAvailableLocales(['fr', 'en'])
    ->setGettextResources('my_domain', '/absolute/path/to/mo_po_resource/directory')
    ->addTranslationCatalog(new FileCatalog('path/to/translations'));
```

Usage anywhere
```php
use ICZones\WPCore\Components\I18n\Translator\Translator;

echo Translator::getInstance()->trans('hello_word'); // Bonjour tout le monde
echo Translator::getInstance()->trans('hello_word_gettext'); // gettext 'Hello world' translation 
echo Translator::getInstance()->trans('nested.example'); // Exemple imbriqué
```

### Frontend translator
A frontend translator allows to use a catalog from JavaScript. 
All that is required is to manually register the [FrontendTranslator](../../src/Components/I18n/FrontendTranslator.php) asset.

Somewhere before rendering a page

```php
use ICZones\WPCore\Components\I18n\FrontendTranslator;
use ICZones\WPCore\Components\Assets\AssetBank;

AssetBank::getInstance()->add(new FrontendTranslator());
```