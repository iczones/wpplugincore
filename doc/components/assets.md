# Assets component
The assets component is used for any script or style registration and queuing.

## Configuration

You can configure the component with the [AssetsConfig](../../src/Components/Assets/AssetsConfig.php) service.

| Method             | Effect                                                                                                   |
| :----------------- | :------------------------------------------------------------------------------------------------------: |
| enable             | Registers the AssetBank to wordpress hooks for loading scripts.                                          |
| enableForLoginPage | Registers the AssetBank to wordpress login hook for loading scripts.                                     |
| addAssetDirectory  | Adds a path from which to search for assets. The ID can now be used in conjunction with AssetPathHelper. |


## Usage
The asset component uses the [AssetBank](../../src/Components/Assets/AssetBank.php) service to register and enqueue any asset class implementing [AssetInterface](../../src/Components/Assets/AssetInterface.php).

To register a new asset you must create a [Script](../../src/Components/Assets/Script.php) or [Stylesheet](../../src/Components/Assets/Stylesheet.php) instance and register/enqueue them with the `AssetBank`. Note that your assets must be accessible from a public directory.


Configuring in the plugin file
```php
use ICZones\WPCore\Components\Assets\AssetsConfig;

AssetsConfig::getInstance()
    ->enable()
    ->enableForLoginPage() // Only if you want to load scripts on the login page
    ->addAssetDirectory('example', '/path/to/assets');
```

Elsewhere when required
```php
use ICZones\WPCore\Components\Assets\Script;
use ICZones\WPCore\Components\Assets\AssetBank;

$script = new Script('my_script_id', '@example/script.js or /absolute/path/to/assets/script.js or public/path/to/assets/script.js');

// Automatically register and enqueue script
AssetBank::getInstance()->add($script);

// Register right now, enqueue later
AssetBank::getInstance()->add($script, false);
AssetBank::getInstance()->enqueue('my_script_id');
```

### Custom assets
If your asset requires some special registration process you can always implement the `AssetInterface`.
```php
use ICZones\WPCore\Components\Assets\AssetPathHelper;

class I18nAsset implements AssetInterface
{
    const ID = 'js_translator';
    
    public function getId(): string
    {
        return self::ID;
    }
    
    public function register()
    {
        Wordpress::registerScript(self::ID, AssetPathHelper::resolveAsset('@example/js-translator.js'));
    }
    
    public function enqueue()
    {
        $translations = array();
        foreach (Locale::getAvailableLocales() as $locale){
            $translations[$locale] = Translator::getInstance()->getDefaultCatalog()->all($locale);
        }
        Wordpress::localizeScript(self::ID, 'translations', $translations);        
        
        Wordpress::enqueueScript(self::ID);
    }
}
```

And then do:
```php
use ICZones\WPCore\Components\Assets\AssetBank;

AssetBank::getInstance()->add(new I18nAsset());
```