# Persistence component
The persistence component gives access to many services and classes to connect models to the database.

## Configuration
You can configure the component with the [PersistenceConfig](../../src/Components/Persistence/PersistenceConfig.php) service.

| Method                | Effect                                                                          |
| :-------------------- | :-----------------------------------------------------------------------------: |
| setPhinxCliFile       | Sets the file used by phinx cli for loading configuration.                      |
| setTablePrefix        | Sets the table prefix for the tables managed by this component.                 |
| addMigrationDirectory | Adds a migration directory in which phinx should check.                         |
| addAdminMigration     | Enables an admin page for managing migrations and adds it to the given submenu. |

## Usage
### Database connection
The [WordpressDB](../../src/Components/Persistence/WordpressDB.php) service is a wrapper around `wpdb` and creates a more conventional way of querying the database.

Since it uses `wpdb` no additional configuration is required.

```php
use ICZones\WPCore\Components\Persistence\WordpressDB;

$success = WordpressDB::getInstance()->execute('query', ['parameterValues']); // Executes a prepared query and replaces given parameters
$value = WordpressDB::getInstance()->fetchScalar(...); // Returns one value or null, same arguments as execute
$row = WordpressDB::getInstance()->fetchSingle(...); // Returns one row or null, same arguments as execute
$table = WordpressDB::getInstance()->fetchMany(...); // Returns an array of rows, same arguments as execute
$id = WordpressDB::getInstance()->lastInsertId(); // Returns the last inserted ID
```
The query argument can also be an instance of `QueryBuilderInterface` or `QueryInterface` from [stefmachine/querybuilder](https://github.com/Stefmachine/query_builder).

### Migrations
This component uses phinx to manage its migrations. Read more about it on [the official site](https://phinx.org/).

You can manage migration states directly from Admin. You only need the `addAdminMigration` configuration.

```php
use ICZones\WPCore\Components\Persistence\PersistenceConfig;

PersistenceConfig::getInstance()
    ->addAdminMigration('my_submenu_slug');
// You can add this before configuring the menu, since it uses async registration
```

### Repository
A simple [GenericRepository](../../src/Components/Persistence/Repository/GenericRepository.php) class can be extended or instantiated to access a specific table data and hydrate an entity with a data mapper.

#### Data mapper
tbr

#### Model Proxy
tbr