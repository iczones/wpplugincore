# Menu component
The menu component wraps the wordpress admin menu functionalities into a builder.

## Configuration
You must use the [MenuConfig](../../src/Components/Menu/MenuConfig.php) to configure the admin menu.

Note that even though the menu is only available in the admin panel, you can safely configure it without checking whether you are in the admin or not.

Since configuration and usage are the same, it will all be explained with an example bellow.

## Usage
```php
use ICZones\WPCore\Components\Menu\MenuConfig;
use ICZones\WPCore\Components\Menu\Submenu;
use ICZones\WPCore\Components\Menu\MenuItem;

MenuConfig::getInstance()
    ->addSubmenu('My menu title', 'my_menu_slug') // Adds a submenu to the admin menu
        ->setCapability('manage_options') // Sets the capability required to see and access the menu. Propagates by default to every items.
        ->setIcon('my-icon-font-icon')
        ->setLabel('My menu label') // Sets the displayed label. Same as the title in the constructor by default
        ->setPosition(0) // The submenu position in the admin menu
        ->setActionHook('action_hook') // The action to execute (actions registered with Wordpress::addAction)
        ->setFunction(function(){}) // A callable is executed instead of an action hook, they are mutually exclusive
        ->redirectTo('my_item_slug') // Tells the submenu to use a submenu item slug instead (like an index page)
        
        
        ->addItem('My item title', 'my_item_slug') // Adds a submenu item
            ->hide() // Hides the submenu item while keeping the submenu active
            // ...
            // Every other methods have the same effect as the submenu's but on the item instead
        ->end() // Back to the submenu
        
        ->addItem('...', '...') // Adding a second item to the submenu
            // ...
        ->end()
        
        ->getItem('my_item_slug') // Gets the submenu item with given slug
            ->setSlug('my_new_item_slug')
        ->end()
    ->end() // Back to the admin menu
        
    ->addSubmenu('...', '...') // Adding a second submenu to the admin menu
    ->end()
    
    ->getSubmenu('my_menu_slug') // Gets the submenu with given slug
        ->setTitle('Changed menu title') // Set
        ->setSlug('changed_menu_slug')
    ->end()
    
     // Registers a submenu configuration that will run once the submenu is created
    ->getSubmenuAsync('my_menu_slug', function(Submenu $_submenu){
        // Do stuff on submenu
        
        // Registers a submenu item configuration that will run once the submenu item is created
        $_submenu->getItemAsync('my_item_slug', function(MenuItem $_item){
            //Do stuff on submenu item
        });
    });
```