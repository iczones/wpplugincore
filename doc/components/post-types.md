# Post Types component
The Post Types component contains classes to build custom post types.

## Configuration
You can configure the component with the [PostTypesConfig](../../src/Components/PostTypes/PostTypesConfig.php) service.

| Method            | Effect                                                        |
| :---------------- | :------------------------------------------------------------ |
| addPostTypes      | Adds post types that will be registered upon initialization.  |
| addTaxonomies     | Adds taxonomies that will be registered upon initialization.  |

## Usage
Configuring in the plugin file
```php
use ICZones\WPCore\Components\PostTypes\PostTypesConfig;

PostTypesConfig::getInstance()
    ->addPostTypes([
        new MyCustomPostTypeExample()
    ])
    ->addTaxonomies([
        new MyCustomTaxonomyExample()
    ]);
```

### Post Type

The [PostType](../../src/Components/PostTypes/PostType.php) class will create a custom post type.

Implementation example
```php
use ICZones\WPCore\Components\PostTypes\PostType;

class MyCustomPostTypeExample extends PostType
{
    const KEY = 'my_custom_post_type_example';
    const SLUG_BASE = 'products';

    public function getKey(): string
    {
        return self::KEY;
    }
    
    public function getSlugBase(): string
    {
        return self::SLUG_BASE;
    }

    public function getLabels(): array
    {
        return $this->getTranslatedLabels('post_type.labels');
    }
    
    public function getCategory(): ?string
    {
        return MyCustomTaxonomyExample::KEY;
    }
    
    public function isPublic(): bool
    {
        return true;
    }

    public function isHierarchical(): bool
    {
        return true;
    }
    
    public function isSlugHierarchical(): bool
    {
        return true;
    }
    
    //...
}
```

| Method               | Effect                                                                               |
| :------------------- | :----------------------------------------------------------------------------------- |
| getKey               | Define the key of the custom post type.                                              |
| getSlugBase          | Define the slug base of the custom post type.                                        |
| getLabels            | Define the labels of the custom post type.                                           |
| getCategory          | Define the category of the custom post type.                                         |
| isPublic             | Whether the post type is intended to use publicly.                                   |
| isHierarchical       | Whether the post type is hierarchical.                                               |
| isSlugHierarchical   | Whether the slug will be prefixed with the category. A category need to be defined.  |
| hasArchive           | Whether the post type has a archive page                                             |
| getMenuIcon          | The url to the icon to be used in the menu.                                          |
| getArgs              | Array of arguments for registering the post type.                                    |
| getTranslatedLabels  | Translate the labels using the Translation class.                                    |

See the `register_post_type` Wordpress function for parameters.

You need to flush the rewrite rules after creating the post type. `Setting > Permalinks > Save Changes`.

`getTranslatedLabels` example:
```php
// translations.fr.php
return [
    'post_type' => [
            'labels' => [
                'name' => '',
                'singular_name' => '',
                'add_new_item' => '',
                'edit_item' => '',
                'new_item' => '',
                'view_item' => '',
                //...
            ]
        ]
];
```

### Taxonomy

The [Taxonomy](../../src/Components/PostTypes/Taxonomy.php) class will create a custom post type taxonomy.

Implementation example
```php
use ICZones\WPCore\Components\PostTypes\Taxonomy;

class MyCustomTaxonomyExample extends Taxonomy
{
    const KEY = 'my_custom_taxonomy_example';
    
    public function getKey(): string
    {
        return self::KEY;
    }
    
    public function getPostTypeKey(): string
    {
        return MyCustomPostTypeExample::KEY;
    }
    
    public function getSlugBase(): string
    {
        return MyCustomPostTypeExample::SLUG_BASE;
    }
    
    public function isHierarchical(): bool
    {
        return true;
    }
    
    //...
}
```

| Method               | Effect                                                                |
| :------------------- | :-------------------------------------------------------------------- |
| getKey               | Define the key of the custom taxonomy.                                |
| getSlugBase          | Define the slug base of the custom taxonomy.                          |
| getPostTypeKey       | PostType key with which the taxonomy should be associated.            |
| getLabels            | Define the labels of the custom taxonomy.                             |
| isPublic             | Whether the taxonomy is intended to use publicly.                     |
| isHierarchical       | Whether the taxonomy is hierarchical.                                 |
| getArgs              | Array of arguments for registering the taxonomy.                      |
| getTranslatedLabels  | Translate the labels using the Translation class (same as PostType).  |

See the `register_taxonomy` Wordpress function for parameters.

You need to flush the rewrite rules after creating the taxonomy. `Setting > Permalinks > Save Changes`.