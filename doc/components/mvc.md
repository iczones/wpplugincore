# MVC component
The MVC component contains many classes to build a MVC workflow.

## Configuration
You can configure the component with the [MvcConfig](../../src/Components/MVC/MvcConfig.php) service.

| Method                    | Effect                                                                                                                           |
| :------------------------ | :------------------------------------------------------------------------------------------------------------------------------: |
| addTemplateDirectory      | Adds a renderer template directory.                                                                                              |
| setTemplateCacheDirectory | Set the renderer template compile cache directory. Since those files are generated they should be put in `WP_CONTENT/uploads/x`. |
| addControllers            | Adds a controller that will be registered upon initialization.                                                                   |
| addShortcodes             | Adds a shortcode that will be registered upon initialization.                                                                    |
| configureRenderer         | Configures the renderer once the renderer is created. Allows definition of extensions and such.                                  |

## Usage
Configuring in the plugin file
```php
use ICZones\WPCore\Components\MVC\MvcConfig;
use Twig\Environment;

MvcConfig::getInstance()
    ->addTemplateDirectory('/absolute/path/to/templates') // Required when using the renderer
    ->setTemplateCacheDirectory('/absolute/path/to/cache') // Optional but optimizes rendering
    ->configureRenderer(function(Environment $_twig){ // Allows deeper configuration
        $_twig->addExtension(...);
    })
    
    // See bellow for controllers and shortcode implementation examples
    ->addControllers([
        new MyActionControllerExample(),
        new MyShortcodeControllerExample(),
    ])
    ->addShortcodes([new MyShortCodeExample()])
    ;
```

### Controllers
With the MVC component, you have access to two types of controllers.

#### ActionHookController
The [ActionHookController](../../src/Components/MVC/Controller/ActionHookController.php) which creates wordpress actions that executes the controller's method.
It can also create ajax actions (private and public) with a default priority of 10.

Implementation example
```php
use ICZones\WPCore\Components\MVC\Controller\ActionHookController;
use ICZones\WPCore\Bridge\Wordpress;

class MyActionControllerExample extends ActionHookController
{
    protected static function getActions() : array
    {
        return [
            'my_ajax_action' => [
                'method' => 'helloWorld', // The controller method
                'post' => true, // Can be executed on form post
                'api' => true, // Can be executed with ajax
                'public' => true, // The ajax/post action can be called without login
                'priority' => 10 // The action execution priority
            ],       
        ];
    }
    
    public function helloWorld()
    {
        // Do controller stuff here
        if(Wordpress::doingAjax()){
            Wordpress::sendJsonSuccess('Hello world!');
        }
        else if(Wordpress::doingFormPost()){
            echo 'Hello world!';
        }
        else{
            echo 'Hello world!';        
        }
    }
}
```

The action can then be used as a normal wordpress action hook by doing a `Wordpress::doAction('my_action')` or with ajax according to wordpress standard way of doing ajax.

The [Menu component](./menu.md) synergizes well with this component since it can use an action hook to display a menu page. 

Note that it is expected for the action to print(`echo`) its content and not to return a string value.

##### Lifecycle

A small lifecycle exists within the `ActionHookController` class. By overriding some methods you can inject yourself at a certain points of execution.

Here is the lifecycle execution order.

| Method       | Effect                                                                                                               | Arguments                                                                       |
| :----------- | :------------------------------------------------------------------------------------------------------------------: | :-----------------------------------------------------------------------------: |
| onInit       | Executed once, the first time the controller is used in the request. Useful for setting up properties and services.  | None                                                                            |
| beforeAction | Executed before any of the controller's actions. The return value (bool) indicates if the action should be executed. | The $_GET params and the action to be executed                                  |
| `theAction`  | Executed if the `beforeAction` returned true.                                                                        | The $_GET params                                                                |
| afterAction  | Executed after any of the controller's actions.                                                                      | The $_GET params, the executed action and the action cancellation status (bool) |                                                                  |

For example

```php
// Within the controller class
function beforeAction(array $_params, string $_action): bool
{
    if(empty($_params['any_$_GET_parameter']) || $_action === 'my_action'){
        return false;
    }
    return true;
}
//...
```

#### Shortcode controller
The [ShortcodeController](../../src/Components/MVC/Controller/ShortcodeController.php) which creates wordpress shortcode that executes the controller's method.

Implementation example
```php
use ICZones\WPCore\Components\MVC\Controller\ShortcodeController;

class MyShortcodeControllerExample extends ShortcodeController
{
    public function getTag() : string
    {
        return 'my-shortcode';
    }

    protected static function getActions() : array
    {
        return [
            'my_shortcode_action' => [
                'method' => 'shortcodeHelloWorld' // The controller method
            ]      
        ];
    }
    
    /**
     * @param array $_params - By default a merge between $_GET and the shortcode attributes, the 'action' key always contains the action name
     */
    public function shortcodeHelloWorld(array $_params)
    {
        // Do controller stuff here
        echo 'Hello world!';
    }
}
```

Then you can use it with:
```html
[my-shortcode action=my_shortcode_action]
```

The `action` attribute is **required**. Without it the controller will simply render and execute nothing.

##### Lifecycle
The shortcode controller also has a lifecycle of its own. It works in similar way to the `ActionHookController` lifecycle.

Here is the lifecycle execution order.

| Method       | Effect                                                                                                               | Arguments                                                                       |
| :----------- | :------------------------------------------------------------------------------------------------------------------: | :-----------------------------------------------------------------------------: |
| onInit       | Executed once, the first time the controller is used in the request. Useful for setting up properties and services.  | None                                                                            |
| beforeAction | Executed before any of the controller's actions. The return value (bool) indicates if the action should be executed. | The $_GET global merged with the shortcode attributes                           |
| `action`     | Executed if the `beforeRender` returned true.                                                                        | The $_GET global merged with the shortcode attributes                           |
| afterAction  | Executed after any of the controller's actions.                                                                      | The $_GET global and the action cancellation status (bool)                      |                                                                  |

This means you will find the `action` among the first parameter values under the 'action' key.

### Custom controller
If you need some other way of working with controllers, you can always implement a custom controller by implementing the [ControllerInterface](../../src/Components/MVC/Controller/ControllerInterface.php).

The `register` method will be called upon wordpress initialization.

## View
### Renderer service
The renderer service is currently a wrapper of `Twig`. Usage is pretty straight forward.

```php
use ICZones\WPCore\Components\MVC\View\RendererService;

$html = RendererService::getInstance()->render('my.twig.template.html.twig', ['data' => 'foo']);
echo $html;
```

Check the configuration to see how to configure and extend the renderer.

### Simple shortcode
Some shortcode implementation already exists to make quick shortcode integration.

```php
use ICZones\WPCore\Components\MVC\MvcConfig;
use ICZones\WPCore\Bridge\Wordpress;
use ICZones\WPCore\Components\MVC\View\CallableShortcode;
use ICZones\WPCore\Components\MVC\View\ActionShortcode;

// Defining a shortcode from a callable
$callableSc = new CallableShortcode('callable-shortcode-tag', function(array $_params){
    return '<p>'.$_params['default'].'</p>';
}, ['default' => 'Hello world!']);

// Defining a shortcode from an action hook
Wordpress::addAction('my_action', function(array $_params){
    echo '<p>'.$_params['default'].'</p>';
});

$actionSc = new ActionShortcode('action-shortcode-tag', 'my_action', ['default' => 'Hello world!']);

// Adding the shortcodes to the configuration
MvcConfig::getInstance()->addShortcodes([$callableSc, $actionSc]);
```

### Custom shortcode
In order to implement a custom shortcode you must extend the [Shortcode](../../src/Components/MVC/View/Shortcode.php) class.

```php
use ICZones\WPCore\Components\MVC\View\Shortcode;

class MyShortCodeExample extends Shortcode
{
    public function getTag() : string
    {
        return 'my-shortcode-example';
    }
    
    protected function render(array $_params, $_content) : string
    {
        return '<p>Hello world!</p>'; // Note here how we return the HTML and don't echo it
    }
    
}
```
And then within a page:
```html
[my-shortcode-example]
```

The shortcode also have a lifecycle. It is the same as the `ShortcodeController`. In fact, it is the `ShortcodeController` which inherits the `Shortcode`'s lifecycle.


### Useful assets
Here are some useful assets to help on the frontend.

| Asset class       | Description                                                                                                                                                          |
| :---------------- | :------------------------------------------------------------------------------------------------------------------------------------------------------------------: |
| [ScopedBootstrap](../../src/Components/MVC/View/Assets/ScopedBootstrap.php)     | Loads bootstrap 4.5 scoped in a `.bootstrap-scope` css class.                                          |
| [Vuejs](../../src/Components/MVC/View/Assets/Vuejs.php)                         | Loads Vue with the specified version from the constructor. Dev mode enabled with `WP_DEBUG`.           |
| [VueMediaComponent](../../src/Components/MVC/View/Assets/VueMediaComponent.php) | Loads the wordpress media wrapped in a Vue component names `wp-media`.                                 |
| [PhpDataParser](../../src/Components/MVC/View/Assets/PhpDataParser.php)         | Loads a Vue directive that can recover json encoded data from the `v-php-data` attribute.              |
| [AjaxApiService](../../src/Components/MVC/View/Assets/AjaxApiService.php)       | Loads an `ApiService` object that can be used to call ajax actions easily. ex: `.get('action', query)` |


## Model
### Traits
With the MVC component, you have access to traits to build your model faster.

| Asset class                                                                  | Description                                                                           |
| :--------------------------------------------------------------------------- | :-----------------------------------------------------------------------------------: |
| [ActiveTrait](../../src/Components/MVC/Model/Common/ActiveTrait.php)         | Trait that adds an active boolean property with its accessors.                        |
| [IdTrait](../../src/Components/MVC/Model/Common/IdTrait.php)                 | Trait that adds an id int property with its accessors.                                |
| [I18nTrait](../../src/Components/MVC/Model/Common/I18nTrait.php)             | Trait that adds a getter that gets a property with the current locale attached to it. |
| [SoftDeleteTrait](../../src/Components/MVC/Model/Common/SoftDeleteTrait.php) | Trait that adds a deleted boolean property with its accessors.                        |
| [TimestampTrait](../../src/Components/MVC/Model/Common/TimestampTrait.php)   | Trait that adds a createdAt and updatedAt datetime properties with their accessors.   |


### View model
To easily json encode your model, you can create a separate view model extending [AbstractViewModel](../../src/Components/MVC/Model/AbstractViewModel.php).

Implementation example
```php
use ICZones\WPCore\Components\MVC\Model\AbstractViewModel;

class Person
{
    protected $name;

    public function __construct(string $_name) 
    {
        $this->name = $_name;
    }
    
    
    public  function getName(){
        return $this->name;
    }
}

class PersonViewModel extends AbstractViewModel
{
    public function toArray(): ?array
    {
        if(!$this->model instanceof Person){
            return null;
        }    
    
        return [
            'name' => $this->model->getName()        
        ];
    }
}

// For a single model
$model = new Person('Jean Paul');
echo json_encode(PersonViewModel::create($model));

// For many models
$manyModels = [
    new Person('Jean Paul'),
    new Person('Jean Bon'),
];
echo json_encode(PersonViewModel::createList($manyModels, function(PersonViewModel $_viewModel){
    // Optional, if you want to process something on each view model
}));
```

### Data mapper
Please refer to the [Persistence component](./persistence.md) for data mapper examples.