# Log component
The log component is used for debugging purposes.

## Configuration
You can configure the component with the [LogConfig](../../src/Components/Log/LogConfig.php) service.

| Method         | Effect                                                                                                  |
| :------------- | :-----------------------------------------------------------------------------------------------------: |
| enable         | Enables the log service. Even disabled, you can still use the log service but nothing will be reported. |

## Usage
With this component, you can use the [Logger](../../src/Components/Log/Logger.php) service to report any error. 
Right now it also requires wordpress `WP_DEBUG` to be true.

```php
use ICZones\WPCore\Components\Log\Logger;

Logger::getInstance()->error("An error occured and you have to fix it.", ['additionnal stuff' => 'imma be json encoded']);
```