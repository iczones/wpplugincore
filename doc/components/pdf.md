# Pdf component
The Pdf component is used to generate Pdfs from HTML templates.

The component requires the installation of [Dompdf](https://github.com/dompdf/dompdf).

## Configuration
You can configure the component with the [PdfConfig](../../src/Components/Pdf/PdfConfig.php) service.

| Method         | Effect                                                                          |
| :------------- | :-----------------------------------------------------------------------------: |
| enable         | Enables the use of HtmlTemplatePdf. If Dompdf is not found, an error is thrown. |

Note that to use this component, you must configure the renderer service from the [MVC component](./mvc.md).

## Usage
```php
use ICZones\WPCore\Components\Pdf\HtmlTemplatePdf;

new HtmlTemplatePdf('template.html.twig', ['param' => 'stuff'], function(Dompdf\Options $_options){
    // Do something about options here
});
```
