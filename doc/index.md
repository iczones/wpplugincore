# Components
The core is split into several components. Each can be configured from the [ComponentsConfig](../src/Config/ComponentsConfig.php) class Singleton service or their respective `<ComponentName>Config` Singleton service.

Here are every component:

* [Assets](./components/assets.md): Assets management for registration and queuing
* [I18n](./components/i18n.md): Translations management with back-end and front-end translators
* [Log](./components/log.md): Logs for debugging
* [Mail](./components/mail.md): Mailing with wordpress made easier
* [Menu](./components/menu.md): Simple admin menu creation
* [MVC](./components/mvc.md): MVC classes workflow
* [Pdf](./components/pdf.md): HTML to pdf generation
* [Persistence](./components/persistence.md): Database connection, repository pattern, data mappers, proxy pattern
* [PostTypes](./components/post-types.md): Post Types management
* [Seo](./components/seo.md): Seo service for sitemap generations
* [Settings](./components/settings.md): Plugin specific mutable settings using database storage
* [Storage](./components/storage.md): Storage and manipulation of private files