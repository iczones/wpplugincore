<?php

return [
    'migrations' => [
        'admin_page' => [
            'title' => 'Versions de la base de données',
            'actions' => [
                'status' => 'Actualiser le statut',
                'migrate' => 'Migrer',
                'rollback' => 'Restaurer la migration',
                'continue' => 'Continuer',
                'cancel' => 'Annuler'
            ],
            'confirm' => [
                'migration' => 'Veuillez confirmer si vous voulez poursuivre le processus de migration.',
                'rollback' => 'Veuillez confirmer si vous voulez poursuivre le processus de restauration.',
            ]
        ]
    ]
];