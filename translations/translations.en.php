<?php

return [
    'migrations' => [
        'admin_page' => [
            'title' => 'Database migrations',
            'actions' => [
                'status' => 'Refresh status',
                'migrate' => 'Migrate',
                'rollback' => 'Rollback',
                'continue' => 'Continue',
                'cancel' => 'Cancel'
            ],
            'confirm' => [
                'migration' => 'Please confirm if you want to continue the migration process.',
                'rollback' => 'Please confirm if you want to continue the rollback process.',
            ]
        ]
    ]
];