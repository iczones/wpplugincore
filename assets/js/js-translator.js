/**
 Translator to translate from JS

 Example usage
 > In template
 <span>{{ 'trans.key'|trans }}</span>...

 > In Vue App
 mounted: function() {
    let text = Translator.trans('trans.key');
 }
 */
(function(_context) {
    function JSTranslator() {
        this.translations = {};
        this.debug = false;
        this.locale = null;
    }

    JSTranslator.prototype.addTranslation = function (_id, _lang, _message) {
        if(!this.translations[_lang]){
            this.translations[_lang] = {};
        }

        this.translations[_lang][_id] = _message;
    };

    JSTranslator.prototype.setTranslations = function (_translations) {
        let $this = this;
        Object.keys(_translations).forEach(function(_lang){
            if(typeof $this.translations[_lang] === "undefined"){
                $this.translations[_lang] = {};
            }
            Object.keys(_translations[_lang]).forEach(function(_id){
                $this.addTranslation(_id, _lang, _translations[_lang][_id]);
            });
        });
    };

    JSTranslator.prototype.trans = function (_id, _params, _locale) {
        if (!_params || typeof _params !== "object") {
            _params = {};
        }

        if (typeof _locale === 'undefined') {
            if (!this.locale) {
                if(this.debug){
                    console.debug("Can't translate without specified locale.");
                }
                return _id;
            }

            _locale = this.locale;
        }

        if(typeof this.translations[_locale][_id] === "undefined"){
            return _id;
        }

        let translation = this.translations[_locale][_id];

        let keys = Object.keys(_params);
        keys.forEach(function (_param) {
            translation = translation.split('%'+_param+'%').join(_params[_param]);
        });

        return translation;
    };

    JSTranslator.prototype.setLocale = function (_locale) {
        this.locale = _locale;
    };

    if(!_context.Translator){
        _context.Translator = new JSTranslator();
        if(_context.iczcorei18n) {
            if(_context.iczcorei18n.locale){
                _context.Translator.setLocale(_context.iczcorei18n.locale);
            }

            if (_context.iczcorei18n.translations) {
                _context.Translator.setTranslations(_context.iczcorei18n.translations);
            }
        }
    }
})(window);
if(typeof Translator !== "undefined" && typeof Vue !== "undefined"){
    Vue.filter('trans', function (_id, _params, _locale) {
        return Translator.trans(_id, _params, _locale)
    });
    Vue.filter('transDefault', function (_id, _default, _params, _locale) {
        let trans = Translator.trans(_id, _params, _locale);
        if(_id === trans){
            return _default;
        }

        return trans;
    });
}