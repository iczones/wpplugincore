/**
 PhpDataService to load data asynchronously from DOM

 Example usage
 > In template
 <div id="my-app" v-php-data="{{ @toJsonAttr(@myControllerData) }}">...

 > In Vue App
 mounted: function() {
       PhpDataService.Data.then((_data) => {
           this.myVueDataValue = _data;
       });
     }
 */
const PhpDataService = (function(){
    class PhpDataService
    {
        constructor(){
            this.data = null;
            this.resolve = null;
            this.promise = new Promise((_resolve) => {
                this.resolve = function(){
                    _resolve(this.data);
                    this.resolve = null;
                };
            });
        }

        get Data()
        {
            return this.promise;
        }

        set Data(_data)
        {
            if(this.resolve){
                this.data = _data;
                this.resolve();
            }
        }
    }

    return new PhpDataService();
})();

if(!Vue){
    console.error('Missing Vue from global context. Vue is required to load the php-data directive of PhpDataService.');
}
else{
    Vue.directive('php-data', {
        bind: function(_el, _binding){
            PhpDataService.Data = _binding.value;
        }
    });
}