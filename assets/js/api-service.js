const ApiService = (function(url, $){
    class ApiService
    {
        constructor(_apiUrl, $)
        {
            this.__apiUrl = _apiUrl;
            this.__$ = $
        }

        get(_action, _query = {}, _config = null)
        {
            return this.__doAjax('GET', _action, _query, null, _config);
        }

        post(_action, _query = {}, _data = {}, _config = null)
        {
            return this.__doAjax('POST', _action, _query, _data, _config);
        }

        put(_action, _query = {}, _data = {}, _config = null)
        {
            return this.__doAjax('PUT', _action, _query, _data, _config);
        }

        patch(_action, _query = {}, _data = {}, _config = null)
        {
            return this.__doAjax('PATCH', _action, _query, _data, _config);
        }

        delete(_action, _query = {}, _config = null)
        {
            return this.__doAjax('DELETE', _action, _query, null, _config);
        }

        head(_action, _query = {}, _config = null)
        {
            return this.__doAjax('HEAD', _action, _query, null, _config);
        }

        options(_action, _query = {}, _config = null)
        {
            return this.__doAjax('OPTIONS', _action, _query, null, _config);
        }

        __doAjax(_method, _action, _query = null, _data = null, _config = null)
        {
            let config = this.__getConfig(_config);
            config.type = _method;
            config.url = this.__getUri(_query);
            config.data = _data || {}

            if (_data instanceof FormData) {
                _data.append('action', _action);
                config.contentType = false;
                config.processData = false;
            } else {
                config.data.action = _action;
            }

            return this.__$.ajax(config);
        }

        /**
         * @private
         * @param _config
         * @returns {Object}
         */
        __getConfig(_config = null)
        {
            let config = _config || {};

            if(typeof config.dataType === "undefined"){
                config.dataType = 'json'
            }

            return config;
        }

        __getUri(_query = null)
        {
            let url = this.__apiUrl;
            let query = (url.split('?')[1] || '');
            query += (query ? '&' : '') + this.__serialize(_query || {});

            return [url, query].join('?');
        }

        __serialize(_params, prefix = '') {
            let string = [];
            for (let param in _params) {
                if (_params.hasOwnProperty(param)) {
                    var key = prefix ? prefix + "[" + param + "]" : param,
                        value = _params[param];
                    string.push(
                        (value !== null && typeof value === "object") ?
                            this.serialize(value, key) :
                            encodeURIComponent(key) + "=" + encodeURIComponent(value)
                    );
                }
            }
            return string.join("&");
        }
    }

    return new ApiService(url, $);
})(wpAjaxUrl.url, jQuery)