Vue.component("wp-media", {
    props: ["add-text", "edit-text", "frame-title", "value", "types"],
    template:
    "<div>" +
        "<button type='button' id='openMedia' class='btn btn-secondary btn-sm'>" +
            "<div v-if='!processing'>" +
                "<span v-if='!selectedMedia'>{{ addText }}</span>" +
                "<span v-else>{{ editText }}</span>" +
            "</div>" +
            "<span v-else class='spinner-border spinner-border-sm'></span>" +
        "</button>" +
        "<div v-if='selectedMedia' class='uploadedFile'>" +
            "<img v-if='isTypeImage()' :src='selectedMedia.url' :alt='selectedMedia.title'>" +
            "<a v-else :href='selectedMedia.url' target='_blank' class='otherFiles'>" +
                "<i class='fas fa-file-alt'></i>" +
                "<span>{{ selectedMedia.filename }}</span>" +
            "</a>" +
        "</div>" +
    "</div>",
    mounted: function() {
        let self = this;

        $("#openMedia").click(function(_e) {
            _e.preventDefault();

            if (self.mediaFrame) {
                self.mediaFrame.open();
                return;
            }

            self.mediaFrame = wp.media({
                title: self.frameTitle,
                library: { type: self.types },
                multiple: false
            });

            self.mediaFrame.on('open', function() {
                let selection = self.mediaFrame.state().get('selection');

                if (self.selectedMedia) {
                    let attachment = wp.media.attachment(self.selectedMedia.id);
                    attachment.fetch();
                    selection.add(attachment ? [attachment] : []);
                }
            });

            self.mediaFrame.on('select', function() {
                self.selectedMedia = self.mediaFrame.state().get('selection').first().toJSON();

                self.$emit("input", self.selectedMedia.id);
            });

            self.mediaFrame.open();
        });
    },
    methods: {
        isTypeImage: function () {
              return this.hasType('image');
        },
        hasType: function (_types) {
            if (Array.isArray(_types)) {
                return this.types.includes(_types);
            } else {
                return this.types === _types;
            }
        }
    },
    watch: {
        value: function(_value) {
            let self = this;

            if (!this.selectedMedia && _value) {
                this.processing = true;
                wp.media.attachment(this.value).fetch().then(function (_data) {
                    self.selectedMedia = _data;
                    self.processing = false;
                });
            }
        }
    },
    data: function() {
        return {
            processing: false,
            mediaFrame: null,
            selectedMedia: null
        }
    }
});