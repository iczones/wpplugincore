<?php


namespace ICZones\WPCore\Common\Date;


use DateTimeImmutable;

class DateHelper
{
    const SEC_PER_MIN = 60;
    const MIN_PER_HOUR = 60;
    const SEC_PER_HOUR = self::SEC_PER_MIN * self::MIN_PER_HOUR;
    const HOUR_PER_DAY = 24;
    const DAY_PER_WEEK = 7;
    
    public static function applyCurrentTimezone(\DateTimeInterface $_date): \DateTimeInterface
    {
        $date = DateTimeImmutable::createFromFormat(DATE_ATOM, $_date->format(DATE_ATOM), $_date->getTimezone());
        return $date->setTimezone(new \DateTimeZone(date_default_timezone_get()));
    }
    
    public static function year(\DateTimeInterface $_date): int
    {
        return $_date->format('Y');
    }
    
    public static function month(\DateTimeInterface $_date): int
    {
        return $_date->format('m');
    }
    
    public static function day(\DateTimeInterface $_date): int
    {
        return $_date->format('d');
    }
    
    /**
     * 0(Sunday) to 6(Saturday)
     *
     * @param \DateTimeInterface $_date
     * @return int
     */
    public static function dayOfWeek(\DateTimeInterface $_date): int
    {
        return $_date->format('w');
    }
    
    public static function isWeekEnd(\DateTimeInterface $_date): bool
    {
        return self::dayOfWeek($_date) % 6 === 0;
    }
}