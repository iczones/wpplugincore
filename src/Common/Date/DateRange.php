<?php


namespace ICZones\WPCore\Common\Date;


class DateRange extends DateTimeRange
{
    public function __construct(\DateTimeInterface $_start, \DateTimeInterface $_end)
    {
        parent::__construct($_start, $_end);
    
        $this->start = $this->start->setTime(0,0,0);
        $this->end = $this->end->setTime(23, 59, 59);
    }
}