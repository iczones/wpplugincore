<?php


namespace ICZones\WPCore\Common\Date;


class DateTimeRange
{
    /** @var bool|\DateTimeImmutable */
    protected $start;
    /** @var bool|\DateTimeImmutable */
    protected $end;
    
    public function __construct(\DateTimeInterface $_start, \DateTimeInterface $_end)
    {
        if($_end < $_start){
            throw new \InvalidArgumentException("Expected end date to be greater than start date.");
        }
        
        $this->start = \DateTimeImmutable::createFromFormat(DATE_ATOM, $_start->format(DATE_ATOM));
        $this->end = \DateTimeImmutable::createFromFormat(DATE_ATOM, $_end->format(DATE_ATOM));
        $this->diff = $this->end->diff($this->start);
    }
    
    public function getStart(): \DateTimeImmutable
    {
        return $this->start;
    }
    
    public function getEnd(): \DateTimeImmutable
    {
        return $this->end;
    }
    
    public function overlaps(DateTimeRange $_range): bool
    {
        return !($_range->getStart() < $this->getStart() && $_range->getEnd() < $this->getStart()
            || $_range->getStart() > $this->getEnd() && $_range->getEnd() > $this->getEnd());
    }
    
    public function getDurationInSeconds(): int
    {
        return $this->end->getTimestamp() - $this->start->getTimestamp();
    }
}