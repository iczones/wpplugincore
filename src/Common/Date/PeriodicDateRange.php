<?php


namespace ICZones\WPCore\Common\Date;


class PeriodicDateRange
{
    protected $startMonth;
    protected $startDay;
    protected $endMonth;
    protected $endDay;
    
    public function __construct(int $_startMonth, int $_startDay, int $_endMonth, int $_endDay)
    {
        $this->startMonth = $_startMonth;
        $this->startDay = $_startDay;
        
        $this->endMonth = $_endMonth;
        $this->endDay = $_endDay;
    }
    
    public static function fromDates(\DateTimeInterface $_start, \DateTimeInterface $_end): PeriodicDateRange
    {
        return new PeriodicDateRange(
            DateHelper::month($_start),
            DateHelper::day($_start),
            DateHelper::month($_end),
            DateHelper::day($_end)
        );
    }
    
    public static function fromRange(DateRange $_range): PeriodicDateRange
    {
        return PeriodicDateRange::fromDates($_range->getStart(), $_range->getEnd());
    }
    
    
    public function getStartMonth(): int {
        return $this->startMonth;
    }
    
    public function getStartDay(): int {
        return $this->startDay;
    }
    
    
    public function getEndMonth(): int {
        return $this->endMonth;
    }
    
    public function getEndDay(): int {
        return $this->endDay;
    }
    
    
    public function rangeOverlaps(DateRange $_range): bool
    {
        list($a, $b, $s, $e) = [
            $this->combine(DateHelper::month($_range->getStart()), DateHelper::day($_range->getStart())), // A - Start of the range
            $this->combine(DateHelper::month($_range->getEnd()), DateHelper::day($_range->getEnd())), // B - End of the range
            $this->combine($this->startMonth, $this->startDay), // S - Start of the periodic range
            $this->combine($this->endMonth, $this->endDay), // E - End of the periodic range
        ];
        
        $b -= $a;
        $s -= $a;
        $e -= $a;
    
        $year = $this->combine(12, 31); // 31st december to loop over a year
        $b = $b < 0 ? $b + $year + 1 : $b;
        $s = $s < 0 ? $s + $year + 1 : $s;
        $e = $e < 0 ? $e + $year + 1 : $e;
        
        // Order should always be A.B.S.E for no overlap
        return !($b < $s && $s <= $e); // Omit A because it's always the smallest at this point
    }
    
    /**
     * Combines a month and a day into a unique integer usable for sorting dates as long as valid dates are given (
     */
    private function combine(int $_month, int $_day): int
    {
        return $_month * 100 + $_day;
    }
}