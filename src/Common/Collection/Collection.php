<?php


namespace ICZones\WPCore\Common\Collection;


class Collection implements \IteratorAggregate, CollectionInterface
{
    use CollectionTypeHelperTrait;
    
    protected $type;
    protected $collection;
    
    public function __construct(string $_type, array $_collection = array())
    {
        $this->type = $_type;
        $this->isValidTypeOrThrow();
        
        $this->collection = array();
        foreach ($_collection as $item){
            $this->add($item);
        }
    }
    
    public function getType(): string
    {
        return $this->type;
    }
    
    public function has($_element)
    {
        return in_array($_element, $this->collection, true);
    }
    
    public function add($_element)
    {
        $this->isValidElementOrThrow($_element);
        if(!$this->has($_element)){
            $this->collection[] = $_element;
        }
    
        return $this;
    }
    
    public function remove($_element)
    {
        $this->isValidElementOrThrow($_element);
        $index = array_search($_element, $this->collection, true);
        if($index !== false){
            array_splice($this->collection, $index, 1); // Removing and resetting indexes
        }
        
        return $this;
    }
    
    public function toArray(): array
    {
        return $this->collection;
    }
    
    public function count()
    {
        return count($this->collection);
    }
    
    public function getIterator()
    {
        foreach ($this->collection as $index => $element){
            yield $index => $element;
        }
    }
}