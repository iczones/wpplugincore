<?php


namespace ICZones\WPCore\Common\Collection;


use InvalidArgumentException;

trait CollectionTypeHelperTrait
{
    protected function isPrimitiveType(): bool
    {
        return in_array($this->getType(), ['integer', 'double', 'boolean', 'string', 'array', 'object', 'NULL']);
    }
    
    /**
     * @param $_element
     * @throws InvalidArgumentException
     * @return bool
     */
    protected function isValidElementOrThrow($_element): bool
    {
        $type = $this->getType();
        if($this->isPrimitiveType()){
            if(gettype($_element) !== $type){
                throw new \InvalidArgumentException("Expecting all elements to of type '{$type}'.");
            }
        }
        else{
            if(!$_element instanceof $type){
                throw new InvalidArgumentException("Expecting all elements to be instances of '{$type}'.");
            }
        }
        
        return true;
    }
    
    /**
     * @throws InvalidArgumentException
     * @return bool
     */
    protected function isValidTypeOrThrow(): bool
    {
        $type = $this->getType();
        if(!$this->isPrimitiveType() && !class_exists($type) && !interface_exists($type)){
            throw new InvalidArgumentException("Class or interface '{$type}' does not exists.");
        }
        
        return true;
    }
}