<?php


namespace ICZones\WPCore\Common\Collection;


interface CollectionInterface extends \Traversable, \Countable
{
    public function add($_element);
    
    public function remove($_element);
    
    public function toArray(): array;
    
    public function getType(): string;
}