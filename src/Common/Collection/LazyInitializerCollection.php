<?php


namespace ICZones\WPCore\Common\Collection;


use ArrayIterator;
use LogicException;

class LazyInitializerCollection implements \IteratorAggregate, CollectionInterface
{
    use CollectionTypeHelperTrait;
    
    /** @var string */
    protected $type;
    /** @var bool */
    protected $initialized;
    /** @var array|null */
    protected $collection;
    /** @var callable */
    protected $initializer;
    /** @var callable|null */
    protected $lazyCounter;
    /** @var int */
    protected $cachedCount;
    
    /** @var callable[] */
    protected $callbacks;
    
    public function __construct(string $_type, callable $_initializer, ?callable $_lazyCounter = null)
    {
        $this->type = $_type;
        $this->isValidTypeOrThrow();
        
        $this->initialized = false;
        $this->collection = null;
        $this->initializer = $_initializer;
        
        $this->lazyCounter = $_lazyCounter;
        $this->cachedCount = null;
        
        $this->callbacks = array();
    }
    
    public function getType(): string
    {
        return $this->type;
    }
    
    protected function initialize()
    {
        if(!$this->initialized){
            $collection = call_user_func($this->initializer);
            if(!is_array($collection) && !$collection instanceof \Traversable){
                throw new LogicException("Initializer must return an array or traversable.");
            }
    
            $this->initialized = true;
            $this->collection = array();
            foreach ($collection as $item){
                $this->add($item);
            }
            
            foreach ($this->callbacks as $callback){
                $this->invoke($callback);
            }
        }
    }
    
    public function add($_element)
    {
        $this->isValidElementOrThrow($_element);
        $this->initialize();
        
        if(!in_array($_element, $this->collection, true)){
            $this->collection[] = $_element;
        }
    }
    
    public function remove($_element)
    {
        $this->isValidElementOrThrow($_element);
        $this->initialize();
    
        $index = array_search($_element, $this->collection, true);
        if($index !== false){
            array_splice($this->collection, $index, 1); // Removing and resetting indexes
        }
    }
    
    public function toArray(): array
    {
        $this->initialize();
        return $this->collection;
    }
    
    public function getIterator()
    {
        return new ArrayIterator($this->toArray());
    }
    
    public function count()
    {
        $this->initialize();
        return count($this->collection);
    }
    
    /**
     * Subscribe the callable to the collection loading for post processing
     * The first argument received is the collection itself
     *
     * @param callable $_callable
     * @return $this
     */
    public function onLoad(callable $_callable)
    {
        if($this->initialized){
            $this->invoke($_callable);
        }
        else{
            $this->callbacks[] = $_callable;
        }
        return $this;
    }
    
    private function invoke(callable $_callable)
    {
        call_user_func($_callable, $this);
    }
    
    public function initialized(): bool
    {
        return $this->initialized;
    }
}