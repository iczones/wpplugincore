<?php

namespace ICZones\WPCore\Common\Helper;

abstract class RequestHelper
{
    const HTTP_POST = 'POST';
    const HTTP_GET = 'GET';
    const HTTP_HEAD = 'HEAD';
    const HTTP_PUT = 'PUT';
    const HTTP_PATCH = 'PATCH';
    const HTTP_OPTIONS = 'OPTIONS';
    
    public static function getMethod()
    {
        return $_SERVER['REQUEST_METHOD'];
    }

    public static function requestMethodIs(string $_method) : bool
    {
        return $_SERVER['REQUEST_METHOD'] === $_method;
    }
    
    public static function isPOST(): bool
    {
        return self::requestMethodIs(self::HTTP_POST);
    }
    
    public static function isGET(): bool
    {
        return self::requestMethodIs(self::HTTP_GET);
    }
    
    public static function isHEAD(): bool
    {
        return self::requestMethodIs(self::HTTP_HEAD);
    }
    
    public static function isPUT(): bool
    {
        return self::requestMethodIs(self::HTTP_PUT);
    }
    
    public static function isPATCH(): bool
    {
        return self::requestMethodIs(self::HTTP_PATCH);
    }
    
    public static function isOPTIONS(): bool
    {
        return self::requestMethodIs(self::HTTP_OPTIONS);
    }
}