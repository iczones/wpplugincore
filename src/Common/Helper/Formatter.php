<?php


namespace ICZones\WPCore\Common\Helper;


use ICZones\WPCore\Components\I18n\Locale;

final class Formatter
{
    public static function currency(?int $_amount, bool $_includeSymbol = true): string
    {
        $amount = $_amount === null ? '' : strval($_amount);
        $amount = str_pad($amount, 3, '0', STR_PAD_LEFT);
        $amount = preg_replace('/^(\d+)(\d{2})$/', '${1}.${2}', $amount);
        
        if($_includeSymbol){
            if(Locale::getCurrent() === 'fr'){
                $amount .= ' $';
            }
            else{
                $amount = '$ '.$amount;
            }
        }
        
        return $amount;
    }
    
    public static function percent(?float $_percent): string
    {
        return (string)(($_percent ?? 0) * 100).'%';
    }
}