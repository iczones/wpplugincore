<?php


namespace ICZones\WPCore;


use ICZones\WPCore\Bridge\Wordpress;
use ICZones\WPCore\Config\ComponentsConfig;


abstract class AbstractWordpressPlugin
{
    private $rootDir;
    private $url;
    private $basename;
    
    public function __construct(string $_bootFile)
    {
        if(!Wordpress::isLoaded()){
            $this->tryInit($_bootFile);
        }
        
        $this->rootDir = Wordpress::plugin_dir_path($_bootFile);
        $this->url = Wordpress::plugin_dir_url($_bootFile);
        $this->basename = Wordpress::plugin_basename($_bootFile);
        
        $this->configure(ComponentsConfig::getInstance());
    }
    
    protected function getPluginRootDirectory()
    {
        return $this->rootDir;
    }
    
    protected function getPluginUrl()
    {
        return $this->url;
    }
    
    protected function getPluginBasename()
    {
        return $this->basename;
    }
    
    private function tryInit(string $_bootFile): bool
    {
        $wpLoad = dirname($_bootFile).'/../../../wp-load.php';
        if(file_exists($wpLoad)){
            require_once($wpLoad);
            return true;
        }
        
        return false;
    }
    
    abstract protected function configure(ComponentsConfig $_configurator);
}