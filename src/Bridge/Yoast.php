<?php


namespace ICZones\WPCore\Bridge;


final class Yoast
{
    private function __construct(){}

    public static function isActive(): bool
    {
        return Wordpress::isPluginActive('wordpress-seo/wp-seo.php') || self::isPremiumActive();
    }
    
    public static function isPremiumActive(): bool
    {
        return Wordpress::isPluginActive('wordpress-seo-premium/wp-seo-premium.php');
    }
    
    public static function getSitemaps()
    {
        global $wpseo_sitemaps;
        return $wpseo_sitemaps;
    }
}