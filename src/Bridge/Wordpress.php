<?php


namespace ICZones\WPCore\Bridge;

use BadMethodCallException;
use WP_Filesystem_Direct;

/**
 * Contains every wordpress global function/constants/variables
 * Makes using wordpress cleaner and allows future compatibility breaks to be fixed here
 *
 * @method static string admin_url(string $path, string $scheme = 'admin')
 * @method static string plugin_dir_path(string $_file)
 * @method static string plugin_dir_url(string $_file)
 * @method static string plugin_basename(string $_file)
 * @method static string untrailingslashit(string $_path)
 * @method static string wp_normalize_path(string $_path)
 */
final class Wordpress
{
    private function __construct(){}
    
    /**
     * Any global function, constant and variable can be called statically
     * The class will resolve it automatically even if it's not currently defined
     * Ex: Wordpress::wpExample()
     *  1. Checks for wpExample as a defined constant
     *  2. Checks for wpExample as a global variable
     *  3. Checks for wpExample as a function
     *  4. Throws BadMethodCallException
     */
    public static function __callStatic($_name, $_arguments)
    {
        if(empty($_arguments)){ // Can be a global
            if(defined($_name)){
                return constant($_name);
            }
            else if(array_key_exists($_name, $GLOBALS)){
                return $GLOBALS[$_name];
            }
        }
        
        if(function_exists($_name)){
            return call_user_func_array($_name, $_arguments);
        }
        
        throw new BadMethodCallException("Wordpress constant, global or function '{$_name}' not found or defined.");
    }
    
    protected static $fs;
    
    public static function isLoaded(): bool
    {
        return defined('ABSPATH');
    }
    
    public static function WP_CONTENT_DIR(): string
    {
        return WP_CONTENT_DIR;
    }
    
    public static function WP_PLUGIN_DIR(): string
    {
        return WP_PLUGIN_DIR;
    }
    
    public static function WP_DEBUG(): bool
    {
        return WP_DEBUG;
    }
    
    /**
     * @param string $_path
     * @param string|null $_scheme
     * @return string
     */
    public static function siteUrl(string $_path = '', string $_scheme = null)
    {
        return site_url($_path, $_scheme);
    }
    
    public static function isAdmin(): bool
    {
        return is_admin();
    }
    
    public static function DB_HOST(): string
    {
        return DB_HOST;
    }
    
    public static function DB_NAME(): string
    {
        return DB_NAME;
    }
    
    public static function DB_USER(): string
    {
        return DB_USER;
    }
    
    public static function DB_PASSWORD(): string
    {
        return DB_PASSWORD;
    }
    
    public static function DB_CHARSET(): string
    {
        return DB_CHARSET;
    }
    
    public static function isPluginActive(string $_plugin)
    {
        if(!function_exists('is_plugin_active')){
            include_once( Wordpress::ABSPATH() . 'wp-admin/includes/plugin.php' );
        }
        
        return is_plugin_active($_plugin);
    }
    
    /**
     * The absolute path to wordpress directory with trailing /
     *
     * @return string
     */
    public static function ABSPATH(): string
    {
        return ABSPATH;
    }

    public static function removeAction(string $_actionHook, callable $_actionReference, int $_priority = 10): bool
    {
        return remove_action($_actionHook, $_actionReference, $_priority);
    }

    public static function addAction(string $_actionHook, callable $_action, int $_priority = 10, int $_acceptedArgsCount = 1): callable
    {
        add_action($_actionHook, $_action, $_priority, $_acceptedArgsCount);
        return $_action;
    }
    
    public static function addAjaxAction(string $_actionHook, callable $_action, bool $_public = false, int $_priority = 10, int $_acceptedArgsCount = 1): callable
    {
        $prefix = !$_public ? 'wp_ajax_' : 'wp_ajax_nopriv_';
        add_action("{$prefix}{$_actionHook}", $_action, $_priority, $_acceptedArgsCount);
        return $_action;
    }
    
    public static function addPostAction(string $_actionHook, callable $_action, bool $_public = false, int $_priority = 10, int $_acceptedArgsCount = 1): callable
    {
        $prefix = !$_public ? 'admin_post_' : 'admin_post_nopriv_';
        add_action("{$prefix}{$_actionHook}", $_action, $_priority, $_acceptedArgsCount);
        return $_action;
    }
    
    public static function doAction(string $_actionHook, ...$_args)
    {
        do_action($_actionHook, ...$_args);
    }
    
    public static function addFilter(string $_filterHook, callable $_action, int $_priority = 10, int $_acceptedArgsCount = 1): callable
    {
        add_filter($_filterHook, $_action, $_priority, $_acceptedArgsCount);
        return $_action;
    }
    
    public static function applyFilters(string $_filter, ...$_values)
    {
        $_values[0] = $_values[0] ?? null;
        return apply_filters($_filter, ...$_values);
    }

    public static function removeFilter(string $filterHook, callable $_actionReference, int $_priority = 10): bool
    {
        return remove_filter($filterHook, $_actionReference, $_priority);
    }

    /**
     * @param string $_scriptId
     * @param string $_src
     * @param string[] $_dependencies
     * @param string|bool|null $_cacheBustVersion
     * @param bool $_inFooter
     */
    public static function enqueueScript(string $_scriptId, string $_src = '', array $_dependencies = array(), $_cacheBustVersion = false, bool $_inFooter = false)
    {
        wp_enqueue_script($_scriptId, $_src, $_dependencies, $_cacheBustVersion, $_inFooter);
    }
    
    /**
     * @param string $_scriptId
     * @param string|bool $_src
     * @param string[] $_dependencies
     * @param string|bool|null $_cacheBustVersion
     * @param bool $_inFooter
     * @return bool
     */
    public static function registerScript(string $_scriptId, $_src, array $_dependencies = array(), $_cacheBustVersion = false, bool $_inFooter = false): bool
    {
        return wp_register_script($_scriptId, $_src, $_dependencies, $_cacheBustVersion, $_inFooter);
    }
    
    /**
     * @param string $_styleId
     * @param string $_src
     * @param string[] $_dependencies
     * @param string|bool|null $_cacheBustVersion
     * @param string $_mediaQuery
     */
    public static function enqueueStyle(string $_styleId, string $_src = '', array $_dependencies = array(), $_cacheBustVersion = false, string $_mediaQuery = 'all' ): void
    {
        wp_enqueue_style($_styleId, $_src, $_dependencies, $_cacheBustVersion, $_mediaQuery);
    }
    
    /**
     * @param string $_styleId
     * @param string|bool $_src
     * @param string[] $_dependencies
     * @param string|bool|null $_cacheBustVersion
     * @param string $_mediaQuery
     * @return bool
     */
    public static function registerStyle(string $_styleId, $_src, array $_dependencies = array(), $_cacheBustVersion = false, string $_mediaQuery = 'all'): bool
    {
        return wp_register_style($_styleId, $_src, $_dependencies, $_cacheBustVersion, $_mediaQuery);
    }
    
    /**
     * @param array $_args
     */
    public static function enqueueMedia(array $_args = array()): void
    {
        wp_enqueue_media($_args);
    }
    
    public static function getLocale(): string
    {
        return get_locale();
    }
    
    public static function sanitizeFileName(string $_fileName)
    {
        return sanitize_file_name($_fileName);
    }
    
    public static function sanitizeTitle(string $_title)
    {
        return sanitize_title($_title);
    }

    public static function getDirectFileSystem()
    {
        if(!self::$fs){
            if(!class_exists('WP_Filesystem_Direct')){
                require_once(Wordpress::ABSPATH().'wp-admin/includes/class-wp-filesystem-base.php');
                require_once(Wordpress::ABSPATH().'wp-admin/includes/class-wp-filesystem-direct.php');
            }
            
            self::$fs = new WP_Filesystem_Direct(false);
        }
        
        return self::$fs;
    }
    
    public static function trailingSlashIt(string $_string)
    {
        return trailingslashit($_string);
    }
    
    public static function getOption(string $_option, $_default = false)
    {
        return get_option($_option, $_default);
    }
    
    public static function wpdb(): \wpdb
    {
        global $wpdb;
        return $wpdb;
    }
    
    public static function WPDB_ARRAY_A()
    {
        return ARRAY_A;
    }
    
    /**
     * @param string $_title
     * @param string $_menuTitle
     * @param string $_capability
     * @param string $_slug
     * @param callable|null $_callback
     * @param string $_iconUrl
     * @param int|null $_position
     * @return string|false
     */
    public static function addMenuPage(string $_title, string $_menuTitle, string $_capability, string $_slug, ?callable $_callback = null, string $_iconUrl = '', int $_position = null)
    {
        return add_menu_page($_title, $_menuTitle, $_capability, $_slug, $_callback ?? '', $_iconUrl, $_position);
    }
    
    /**
     * @param string $_parentMenuSlug
     * @param string $_title
     * @param string $_menuTitle
     * @param string $_capability
     * @param string $_slug
     * @param callable|null $function
     * @param int|null $position
     * @return string|false
     */
    public static function addSubmenuPage(string $_parentMenuSlug, string $_title, string $_menuTitle, string $_capability, string $_slug, ?callable $function = null, int $position = null)
    {
        return add_submenu_page($_parentMenuSlug, $_title, $_menuTitle, $_capability, $_slug, $function ?? '', $position);
    }
    
    /**
     * @param string|array $_to
     * @param string $_subject
     * @param string $_message
     * @param string|array $_headers
     * @param string|array $_attachments
     * @return bool
     */
    public static function mail($_to, string $_subject, string $_message, $_headers = '', $_attachments = array()): bool
    {
        return wp_mail($_to, $_subject, $_message, $_headers, $_attachments);
    }
    
    public static function sendJsonSuccess($_data = null, ?int $_status = null, int $_jsonEncodeFlags = 0): void
    {
        wp_send_json_success($_data, $_status, $_jsonEncodeFlags);
    }
    
    public static function sendJsonError($_data = null, ?int $_status = null, int $_jsonEncodeFlags = 0): void
    {
        wp_send_json_error($_data, $_status, $_jsonEncodeFlags);
    }
    
    /**
     * @param string $_domain
     * @param string|false $_pluginRelativePath
     * @return bool
     */
    public static function loadPluginTextDomain(string $_domain, $_pluginRelativePath = false): bool
    {
        return load_plugin_textdomain($_domain, false, $_pluginRelativePath);
    }
    
    public static function localizeScript(string $_scriptId, string $_javascriptVar, array $_data): bool
    {
        return wp_localize_script($_scriptId, $_javascriptVar, $_data);
    }
    
    public static function statusHeader(int $_statusCode, string $_description = ''): void
    {
        status_header($_statusCode, $_description);
    }
    
    /**
     * @param string $_slug
     * @param string|null $_name
     * @param array $_args
     * @return bool
     */
    public static function getTemplatePart(string $_slug, ?string $_name = null, array $_args = array()): bool
    {
        return get_template_part($_slug, $_name, $_args) !== false;
    }
    
    public static function addShortcode(string $_tag, callable $_callback): void
    {
        add_shortcode($_tag, $_callback);
    }
    
    public static function shortcodeAtts(array $_default, array $_attributes, string $_tag = ''): array
    {
        return shortcode_atts($_default, $_attributes, $_tag);
    }
    
    public static function doingAjax(): bool
    {
        return wp_doing_ajax();
    }
    
    public static function doingFormPost(): bool
    {
        return Wordpress::isAdmin() && basename($_SERVER['SCRIPT_FILENAME']) === 'admin-post.php';
    }

	public static function registerPostType(string $_key, array $_args = [])
	{
		register_post_type($_key, $_args);
	}
	public static function registerTaxonomy(string $_key, string $_postTypeKey, array $_args = [])
	{
		register_taxonomy($_key, $_postTypeKey, $_args);
	}
}