<?php


namespace ICZones\WPCore\Bridge;


final class Wpml
{
    private function __construct(){}
    
    public static function isActive(): bool
    {
        return Wordpress::isPluginActive('sitepress-multilingual-cms/sitepress.php'); // According to their documentation
    }
    
    public static function isStringTranslationActive(): bool
    {
        return Wordpress::isPluginActive('wpml-string-translation/plugin.php');
    }
    
    public static function isMediaTranslationActive(): bool
    {
        return Wordpress::isPluginActive('wpml-media-translation/plugin.php');
    }
    
    public static function isTranslationManagementActive(): bool
    {
        return Wordpress::isPluginActive('wpml-translation-management/plugin.php');
    }
    
    public static function getCurrentLanguage(): ?string
    {
        return Wordpress::applyFilters('wpml_current_language');
    }
}