<?php


namespace ICZones\WPCore\Components\Log;


use ICZones\WPCore\Components\ServiceLayer\SingletonTrait;

class LogConfig
{
    use SingletonTrait;
    
    protected $enabled;
    
    public function __construct()
    {
        $this->enabled = false;
    }
    
    /**
     * Enables the log service, you can still use the log service but nothing will be reported
     */
    public function enable()
    {
        $this->enabled = true;
    }
    
    public function isEnabled(): bool
    {
        return $this->enabled;
    }
}