<?php


namespace ICZones\WPCore\Components\Log;


use ICZones\WPCore\Bridge\Wordpress;
use ICZones\WPCore\Components\ServiceLayer\SingletonTrait;

class Logger
{
    use SingletonTrait;
    
    public function error(string $_message, array $_data = array())
    {
        if($this->isActive()){
            @error_log(date('Y-m-d H:i:s').': [ERROR] '.$_message.' ['.@json_encode($_data).']');
        }
    }
    
    private function isActive(): bool
    {
        return LogConfig::getInstance()->isEnabled();
    }
}