<?php


namespace ICZones\WPCore\Components\Assets;

/**
 * @internal
 */
interface CoreAssetInterface
{
    const BASE_ASSET_ID = 'icz_wpcore_';
}