<?php


namespace ICZones\WPCore\Components\Assets;


use ICZones\WPCore\Bridge\Wordpress;
use ICZones\WPCore\Components\ServiceLayer\SingletonTrait;

class AssetsConfig
{
    use SingletonTrait;
    
    protected $assetDirectories;
    
    public function __construct()
    {
        $this->assetDirectories = array();
        $this->addAssetDirectory('iczcore', __DIR__.'/../../../assets');
    }
    
    /**
     * Registers the AssetBank to wordpress hooks for loading scripts
     */
    public function enable()
    {
        $hooks = ['admin_enqueue_scripts', 'wp_enqueue_scripts'];
        foreach ($hooks as $hook){
            Wordpress::addAction($hook, function(){
                AssetBank::getInstance()->process();
            });
        }
        
        return $this;
    }
    
    /**
     * Registers the AssetBank to wordpress login hook for loading scripts
     *
     * @return $this
     */
    public function enableForLoginPage()
    {
        Wordpress::addAction('login_enqueue_scripts', function(){
            AssetBank::getInstance()->process();
        });
        
        return $this;
    }
    
    /**
     * Adds a path from which to search for assets. The ID can now be used in conjunction with AssetPathHelper
     *
     * @param string $_id
     * @param string $_absolutePath
     */
    public function addAssetDirectory(string $_id, string $_absolutePath)
    {
        if(file_exists($_absolutePath)){
            $this->assetDirectories[$_id] = Wordpress::untrailingslashit(Wordpress::wp_normalize_path(realpath($_absolutePath)));
        }
    }
    
    public function getAssetDirectories(): array
    {
        return $this->assetDirectories;
    }
}