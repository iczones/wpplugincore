<?php


namespace ICZones\WPCore\Components\Assets;


use ICZones\WPCore\Bridge\Wordpress;

class AssetPathHelper
{
    public static function resolveAsset(string $_path)
    {
        if(preg_match('/^@(\w+)/', $_path, $matches)){
            $id = $matches[1];
            $dirs = AssetsConfig::getInstance()->getAssetDirectories();
            if(!empty($dirs[$id])){
                $_path = str_replace("@{$id}", $dirs[$id], $_path);
            }
        }
        
        return self::resolveFileToUrlPath($_path);
    }
    
    public static function resolveFileToUrlPath(string $_path): string
    {
        if(file_exists($_path)){
            $uri = str_replace(Wordpress::wp_normalize_path(Wordpress::ABSPATH()), '', Wordpress::wp_normalize_path(realpath($_path)));
            $prefix = substr($uri, 0, 1) !== '/' ? '/' : '';
            return $prefix.$uri;
        }
    
        return $_path;
    }
}