<?php


namespace ICZones\WPCore\Components\Assets;


use ICZones\WPCore\Bridge\Wordpress;

class Script extends Asset
{
    protected $inFooter;
    
    public function __construct(string $_id, string $_src)
    {
        $this->inFooter = false;
        parent::__construct($_id, $_src);
    }
    
    public function setInFooter(bool $_inFooter)
    {
        $this->inFooter = $_inFooter;
        return $this;
    }
    
    public function register()
    {
        Wordpress::registerScript($this->id, $this->src, $this->dependencies, $this->cacheBuster, $this->inFooter);
    }
    
    public function enqueue()
    {
        Wordpress::enqueueScript($this->id);
    }
}