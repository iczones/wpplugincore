<?php


namespace ICZones\WPCore\Components\Assets;


interface AssetInterface
{
    public function getId(): string;
    public function register();
    public function enqueue();
}