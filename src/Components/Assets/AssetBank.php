<?php


namespace ICZones\WPCore\Components\Assets;


use ICZones\WPCore\Bridge\Wordpress;
use ICZones\WPCore\Components\Log\Logger;
use ICZones\WPCore\Components\ServiceLayer\SingletonTrait;

/**
 * Any assets added to the bank are automatically registered and enqueued
 */
class AssetBank
{
    /** @var AssetInterface[] */
    protected $assets;
    protected $assetsQueue;
    
    protected $processed;
    
    use SingletonTrait;
    
    public function __construct()
    {
        $this->assets = [];
        $this->assetsQueue = [];
        $this->processed = false;
    }
    
    /**
     * Processes the registered scripts
     *
     * @internal
     */
    public function process()
    {
        if(!$this->processed){
            foreach ($this->assets as $asset){
                $asset->register();
            }
    
            foreach ($this->assetsQueue as $asset){
                $asset->enqueue();
            }
    
            $this->processed = true;
        }
    }
    
    public function add(AssetInterface $_asset, bool $_enqueue = true)
    {
        $this->assets[$_asset->getId()] = $_asset;
    
        if($this->processed){
            $_asset->register();
        }
        
        if($_enqueue){
            $this->enqueue($_asset->getId());
        }
        
        return $this;
    }
    
    public function enqueue(string $_assetId)
    {
        if(empty($this->assets[$_assetId])){
            Logger::getInstance()->error("Attempting to enqueue unregistered asset '{$_assetId}'.");
        }
        else{
            $this->assetsQueue[$_assetId] = $this->assets[$_assetId];
            
            if($this->processed){
                $this->assetsQueue[$_assetId]->enqueue();
            }
        }
    
        return $this;
    }
}