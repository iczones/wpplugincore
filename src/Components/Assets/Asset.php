<?php


namespace ICZones\WPCore\Components\Assets;




abstract class Asset implements AssetInterface
{
    protected $id;
    protected $src;
    protected $type;
    protected $dependencies;
    protected $cacheBuster;
    
    public function __construct(string $_id, string $_src)
    {
        $this->id = $_id;
        $this->src = AssetPathHelper::resolveAsset($_src);
        $this->dependencies = array();
        $this->inFooter = false;
        $this->cacheBuster = false;
    }
    
    public function getId(): string
    {
        return $this->id;
    }
    
    public function getSrc(): string
    {
        return $this->src;
    }
    
    /**
     * @return string[]
     */
    public function getDependencies(): array
    {
        return $this->dependencies;
    }
    
    public function dependsOn(string ...$_ids)
    {
        $this->dependencies = array_unique(array_merge($this->dependencies, $_ids));
        return $this;
    }
    
    public function useDefaultCacheBuster()
    {
        $this->cacheBuster = false;
        return $this;
    }
    
    public function useCustomCacheBuster(string $_version)
    {
        $this->cacheBuster = $_version;
        return $this;
    }
    
    public function useNoCacheBuster()
    {
        $this->cacheBuster = null;
        return $this;
    }
}