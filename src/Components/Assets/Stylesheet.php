<?php


namespace ICZones\WPCore\Components\Assets;


use ICZones\WPCore\Bridge\Wordpress;

class Stylesheet extends Asset
{
    protected $mediaQuery;
    
    public function __construct(string $_id, string $_src)
    {
        parent::__construct($_id, $_src);
        
        $this->mediaQuery = 'all';
    }
    
    public function setMediaQuery(string $_mediaQuery)
    {
        $this->mediaQuery = $_mediaQuery;
        return $this;
    }
    
    public function register()
    {
        Wordpress::registerStyle($this->id, $this->src, $this->dependencies, $this->cacheBuster, $this->mediaQuery);
    }
    
    public function enqueue()
    {
        Wordpress::enqueueStyle($this->id);
    }
}