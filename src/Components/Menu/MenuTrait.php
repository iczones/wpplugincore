<?php


namespace ICZones\WPCore\Components\Menu;


/**
 * @internal
 */
trait MenuTrait
{
    /** @var string */
    protected $label;
    
    /** @var string */
    protected $title;
    
    /** @var string */
    protected $slug;
    
    /** @var string */
    protected $capability;
    
    /** @var callable|null */
    protected $function;
    
    /** @var string|null */
    protected $actionHook;
    
    /** @var string */
    protected $icon;
    
    /** @var int|null */
    protected $position;
    
    public function isCurrent(): bool
    {
        return isset($_GET['page']) && $this->slug == $_GET['page'];
    }
    
    /**
     * Sets the displayed label. Same as the title in the constructor by default
     *
     * @param string $_label
     * @return $this
     */
    public function setLabel(string $_label)
    {
        $this->label = $_label;
        return $this;
    }
    
    public function getLabel(): string
    {
        return $this->label;
    }
    
    /**
     * Sets the page title
     *
     * @param string $_title
     * @return $this
     */
    public function setTitle(string $_title)
    {
        $this->title = $_title;
        return $this;
    }
    
    public function getTitle(): string
    {
        return $this->title;
    }
    
    /**
     * Sets the page slug
     *
     * @param string $_slug
     * @return $this
     */
    public function setSlug(string $_slug)
    {
        $this->slug = $_slug;
        return $this;
    }
    
    public function getSlug(): string
    {
        return $this->slug;
    }
    
    /**
     * Sets the capability required to see and access the menu
     *
     * @param string $_capability
     * @return $this
     */
    public function setCapability(string $_capability)
    {
        $this->capability = $_capability;
        return $this;
    }
    
    public function getCapability(): string
    {
        return $this->capability;
    }
    
    /**
     * A callable is executed instead of an action hook, they are mutually exclusive
     *
     * @param callable|null $_function
     * @return $this
     */
    public function setFunction(?callable $_function)
    {
        $this->function = $_function;
        return $this;
    }
    
    /**
     * The action to execute (actions registered with Wordpress::addAction)
     *
     * @param string $_actionHook
     * @return $this
     */
    public function setActionHook(string $_actionHook)
    {
        $this->actionHook = $_actionHook;
        return $this;
    }
    
    public function setIcon(string $_icon)
    {
        $this->icon = $_icon;
        return $this;
    }
    
    public function getIcon(): string
    {
        return $this->icon;
    }
    
    public function setPosition(?int $_position)
    {
        $this->position = $_position;
        return $this;
    }
    
    public function getPosition(): ?int
    {
        return $this->position;
    }
}