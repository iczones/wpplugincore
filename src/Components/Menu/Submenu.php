<?php


namespace ICZones\WPCore\Components\Menu;


use ICZones\WPCore\Bridge\Wordpress;

/**
 * @internal
 */
class Submenu
{
    use MenuTrait;
    
    /** @var MenuConfig */
    protected $menu;
    /** @var MenuItem[] */
    protected $items;
    
    /** @var string|null */
    protected $redirectItemSlug;
    /** @var bool */
    protected $registered;
    
    /** @var callable[][] */
    protected $asyncGets;
    
    public function __construct(MenuConfig $_menu, string $_title, string $_slug)
    {
        $this->menu = $_menu;
        $this->items = array();
        $this->registered = false;
        $this->asyncGets = array();
     
        $this->setTitle($_title)
            ->setLabel($_title)
            ->setSlug($_slug)
            ->setIcon('')
            ->setCapability('manage_options');
    }
    
    /**
     * Adds a submenu item
     *
     * @param string $_title
     * @param string $_slug
     * @return MenuItem
     */
    public function addItem(string $_title, string $_slug): MenuItem
    {
        $item = new MenuItem($this, $_title, $_slug);
        $this->items[$_slug] = $item;
        return $item;
    }
    
    public function getItems(): array
    {
        return $this->items;
    }
    
    /**
     * Gets the submenu item with given slug
     *
     * @param string $_slug
     * @return MenuItem|null
     */
    public function getItem(string $_slug): ?MenuItem
    {
        return $this->items[$_slug] ?? null;
    }
    
    /**
     * Registers a callable that will resolve once the submenu item is created or right away if it already exists
     *
     * @param string $_slug
     * @param callable $_onResolve
     * @return $this
     */
    public function getItemAsync(string $_slug, callable $_onResolve)
    {
        if(!isset($this->asyncGets[$_slug])){
            $this->asyncGets[$_slug] = array();
        }
        
        $this->asyncGets[$_slug][] = $_onResolve;
        
        if($item = $this->getItem($_slug)){
            $this->resolveAsyncGets($item);
        }
        
        return $this;
    }
    
    private function resolveAsyncGets(MenuItem $_item)
    {
        if(!empty($this->asyncGets[$_item->getSlug()])){
            foreach($this->asyncGets[$_item->getSlug()] as $index => $callable){
                call_user_func($callable, $_item);
                unset($this->asyncGets[$_item->getSlug()][$index]);
            }
        }
    }
    
    /**
     * Sets the capability required to see and access the menu. Propagates by default to every items.
     *
     * @param string $_capability
     * @param bool $_propagate
     * @return $this
     */
    public function setCapability(string $_capability, bool $_propagate = true)
    {
        $this->capability = $_capability;
        
        if($_propagate){
            foreach ($this->items as $item){
                $item->setCapability($_capability);
            }
        }
        
        return $this;
    }
    
    /**
     * Tells the submenu to use an item slug instead (like an index page)
     *
     * @param string $_itemSlug
     * @return $this
     */
    public function redirectTo(string $_itemSlug)
    {
        $this->redirectItemSlug = $_itemSlug;
        return $this;
    }
    
    public function isRedirected(): bool
    {
        return $this->redirectItemSlug !== null;
    }
    
    public function getRedirectSlug(): ?string
    {
        return $this->redirectItemSlug;
    }
    
    public function getAction(): ?callable
    {
        if($this->function){
            return $this->function;
        }
        
        if($this->actionHook){
            return function(){
                Wordpress::doAction($this->actionHook);
            };
        }
        
        return null;
    }
    
    /**
     * Back to the admin menu
     *
     * @return MenuConfig
     */
    public function end(): MenuConfig
    {
        return $this->menu;
    }
    
    /**
     * @internal
     */
    public function register()
    {
        if(!$this->registered){
            Wordpress::addMenuPage(
                $this->title,
                $this->label,
                $this->capability,
                $this->redirectItemSlug ?? $this->slug,
                $this->getAction(),
                $this->icon,
                $this->position
            );
            
            $this->registered = true;
        }
        
        $orderedItems = array_values($this->items);
    
        usort($orderedItems, function(MenuItem $_a, MenuItem $_b){
            $a = $_a->getPosition() ?? ($_b->getPosition() ?? 0) + 1;
            $b = $_b->getPosition() ?? ($_a->getPosition() ?? 0) + 1;
        
            return $a <=> $b;
        });
        
        foreach ($orderedItems as $items){
            $items->register();
        }
        
        return $this;
    }
}