<?php


namespace ICZones\WPCore\Components\Menu;


use ICZones\WPCore\Components\Assets\Stylesheet;
use ICZones\WPCore\Components\Assets\CoreAssetInterface;

/**
 * @internal
 */
class HiddenMenuItemStyle extends Stylesheet implements CoreAssetInterface
{
    const ID = self::BASE_ASSET_ID.'hide_active_submenu';
    
    public function __construct()
    {
        parent::__construct(self::ID, '@iczcore/css/hide-menu.css');
    }
}