<?php


namespace ICZones\WPCore\Components\Menu;


use ICZones\WPCore\Components\Assets\AssetBank;
use ICZones\WPCore\Bridge\Wordpress;

/**
 * @internal
 */
class MenuItem
{
    use MenuTrait;
    
    /** @var Submenu */
    protected $menu;
    
    /** @var bool */
    protected $hidden;
    
    /** @var bool */
    protected $registered;
    
    public function __construct(Submenu $_menu, string $_title, string $_slug)
    {
        $this->menu = $_menu;
        $this->hidden = false;
        $this->registered = false;
        
        $this->setTitle($_title)
            ->setLabel($_title)
            ->setSlug($_slug)
            ->setIcon('')
            ->setCapability($_menu->getCapability());
    }
    
    /**
     * Hides the submenu item while keeping the submenu active
     *
     * @return $this
     */
    public function hide()
    {
        $this->hidden = true;
        return $this;
    }
    
    public function isHidden(): bool
    {
        return $this->hidden;
    }
    
    public function getAction(): ?callable
    {
        if($this->actionHook){
            return function(){
                if($this->isHidden()){
                    // Hides the currently active menu with css style if it is supposed to be hidden
                    // while keeping the parent menu collapse open and active
                    AssetBank::getInstance()->add(new HiddenMenuItemStyle(), true);
                }
                
                if($this->function){
                    call_user_func($this->function);
                }
                else{
                    Wordpress::doAction($this->actionHook);
                }
            };
        }
        
        return null;
    }
    
    /**
     * Back to the submenu
     *
     * @return Submenu
     */
    public function end()
    {
        return $this->menu;
    }
    
    /**
     * @internal
     */
    public function register()
    {
        if(!$this->registered){
            Wordpress::addSubmenuPage(
                $this->hidden && !$this->isCurrent() ? '' : $this->menu->getSlug(),
                $this->title,
                $this->label,
                $this->capability,
                $this->slug,
                $this->getAction(),
                $this->getPosition()
            );
            $this->registered = true;
        }
        return $this;
    }
}