<?php


namespace ICZones\WPCore\Components\Menu;


use ICZones\WPCore\Bridge\Wordpress;
use ICZones\WPCore\Components\ServiceLayer\SingletonTrait;

/**
 * The Wordpress admin menu as a configurable service
 */
class MenuConfig
{
    use SingletonTrait;
    
    /** @var Submenu[] */
    protected $submenus;
    protected $asyncGets;
    
    public function __construct()
    {
        $this->asyncGets = array();
        $this->submenus = array();
        
        Wordpress::addAction('admin_menu', function () {
            foreach ($this->submenus as $submenu){
                $submenu->register();
            }
        });
    }
    
    /**
     * Adds a submenu to the admin menu
     *
     * @param string $_title
     * @param string $_slug
     * @return Submenu
     */
    public function addSubmenu(string $_title, string $_slug): Submenu
    {
        $submenu = new Submenu($this, $_title, $_slug);
        $this->submenus[$_slug] = $submenu;
        
        $this->resolveAsyncGets($submenu);
        
        return $submenu;
    }
    
    /**
     * Gets the submenu with given slug
     *
     * @param string $_slug
     * @return Submenu|null
     */
    public function getSubmenu(string $_slug): ?Submenu
    {
        return $this->submenus[$_slug] ?? null;
    }
    
    /**
     * Registers a callable that will resolve once the submenu is created or right away if it already exists
     *
     * @param string $_slug
     * @param callable $_onResolve
     * @return $this
     */
    public function getSubmenuAsync(string $_slug, callable $_onResolve)
    {
        if(!isset($this->asyncGets[$_slug])){
            $this->asyncGets[$_slug] = array();
        }
        
        $this->asyncGets[$_slug][] = $_onResolve;
        
        if($submenu = $this->getSubmenu($_slug)){
            $this->resolveAsyncGets($submenu);
        }
        
        return $this;
    }
    
    private function resolveAsyncGets(Submenu $_submenu)
    {
        if(!empty($this->asyncGets[$_submenu->getSlug()])){
            foreach($this->asyncGets[$_submenu->getSlug()] as $index => $callable){
                call_user_func($callable, $_submenu);
                unset($this->asyncGets[$_submenu->getSlug()][$index]);
            }
        }
    }
}