<?php


namespace ICZones\WPCore\Components\MVC\Controller;


use ICZones\WPCore\Bridge\Wordpress;
use ICZones\WPCore\Components\Log\Logger;

abstract class ActionHookController implements ControllerInterface
{
    use ControllerTrait;
    
    /**
     * [<action_name> => [
     *   'method' => string, - The controller method
     *   'post' => bool, - [OPTIONAL: false] If the action can be triggered from a form post
     *   'api' => bool, - [OPTIONAL: false] If the action can be called via ajax
     *   'public' => bool, - [OPTIONAL: false] If the ajax/post action can be called without login
     *   'priority' => int - [OPTIONAL: 10] The action priority
     * ]]
     *
     * @return array[]
     */
    abstract protected static function getActions(): array;
    
    // LIFECYCLE
    /**
     * Process anything that needs to be initiated before a route call from this controller
     * Like setting up services or controller properties
     *
     * @return void
     */
    protected function onInit(){}
    private $initialized = false;
    
    /**
     * Called right before the controller's action
     *
     * Returning false will prevent the action from happening and
     * will also set the $_canceled params in the after hook to TRUE
     *
     * @param array $_params
     * @param string $_action
     * @return boolean
     */
    protected function beforeAction(array $_params, string $_action): bool { return true; }
    
    /**
     * Called right after the controller's action
     *
     * @param array $_params
     * @param string $_action
     * @param bool $_canceled
     * @return void
     */
    protected function afterAction(array $_params, string $_action, bool $_canceled): void {}
    // END OF LIFECYCLE
    
    /**
     * Register the actions hook into wordpress lifecycle
     */
    public function register()
    {
        foreach (static::getActions() as $hook => $action){
            $function = function() use ($action){
                if(!$this->initialized){
                    $this->onInit();
                    $this->initialized = true;
                }
                
                $params = $_GET;
                $method = $action['method'] ?? '';
                if(empty($method) || !method_exists($this, $method)){
                    Logger::getInstance()->error("Given action method '{$method}' was not found in controller ".static::class.".");
                }
                else{
                    $continue = $this->beforeAction($params, $method);
                    if($continue){
                        call_user_func([$this, $method], $params);
                    }
                    $this->afterAction($params, $method, !$continue);
                }
            };
            
            $priority = $action['priority'] ?? 10;
            Wordpress::addAction($hook, $function, $priority);
            if($action['api'] ?? false){
                Wordpress::addAjaxAction($hook, $function, false, $priority);
                if($action['public'] ?? false){
                    Wordpress::addAjaxAction($hook, $function, true, $priority);
                }
            }
            
            if($action['post'] ?? false){
                Wordpress::addPostAction($hook, $function, false, $priority);
                if($action['public'] ?? false){
                    Wordpress::addPostAction($hook, $function, true, $priority);
                }
            }
        }
    }
}