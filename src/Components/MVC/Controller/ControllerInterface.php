<?php


namespace ICZones\WPCore\Components\MVC\Controller;


interface ControllerInterface
{
    public function register();
}