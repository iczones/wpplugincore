<?php


namespace ICZones\WPCore\Components\MVC\Controller;


use ICZones\WPCore\Components\MVC\View\RendererService;

trait ControllerTrait
{
    public function render(string $_template, array $_context = array()): string
    {
        return RendererService::getInstance()->render($_template, $_context);
    }
}