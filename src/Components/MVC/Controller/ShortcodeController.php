<?php


namespace ICZones\WPCore\Components\MVC\Controller;


use ICZones\WPCore\Components\Log\Logger;
use ICZones\WPCore\Components\MVC\View\Shortcode;

abstract class ShortcodeController extends Shortcode implements ControllerInterface
{
    use ControllerTrait {
        render as renderTemplate;
    }
    
    /**
     * [<action_name> => [
     *   'method' => string, - The controller method
     * ]]
     *
     * @return array[]
     */
    abstract protected static function getActions(): array;
    
    protected function getDefaultParams(): array
    {
        $params = $_GET;
        $params['action'] = null; // Forces getting action from attributes
        return $params;
    }
    
    final protected function render(array $_params, $_content): string
    {
        if(empty($_params['action'])){
            Logger::getInstance()->error('Using shortcode controller without specifying action.');
            return '';
        }
        $actions = static::getActions();
        $action = $actions[$_params['action']] ?? null;
        
        if(!$action){
            Logger::getInstance()->error("Shortcode action '{$action}' does not exist in controller '".static::class."'.");
            return '';
        }
    
        $method = $action['method'] ?? '';
        if(empty($method) || !method_exists($this, $method)){
            Logger::getInstance()->error("Given shortcode action method '{$method}' was not found in controller ".static::class.".");
        }
        
        ob_start();
        // Using echo instead of returning a string, making it work more like a normal controller
        // Also gives more control on what and when to echo
        call_user_func([$this, $method], $_params, $_content);
        return ob_get_clean();
    }
}