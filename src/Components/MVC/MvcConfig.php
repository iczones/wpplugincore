<?php


namespace ICZones\WPCore\Components\MVC;


use ICZones\WPCore\Bridge\Wordpress;
use ICZones\WPCore\Components\MVC\Controller\ControllerInterface;
use ICZones\WPCore\Components\MVC\View\Shortcode;
use ICZones\WPCore\Components\ServiceLayer\SingletonTrait;
use InvalidArgumentException;
use LogicException;

class MvcConfig
{
    use SingletonTrait;
    
    protected $templateDirectories;
    protected $cacheDirectory;
    protected $controllers;
    protected $shortcodes;
    protected $rendererExtensions;
    protected $init;
    
    public function __construct()
    {
        $this->controllers = array();
        $this->shortcodes = array();
        $this->templateDirectories = array();
        $this->rendererExtensions = array();
        
        $this->init = false;
        Wordpress::addAction('init', function(){
            foreach ($this->getControllers() as $controller){
                $controller->register();
            }
        
            foreach ($this->getShortCodes() as $shortcode){
                $shortcode->register();
            }
            
            $this->init = true;
        });
    }
    
    /**
     * Adds a renderer template directory
     *
     * @param string $_templateDirectory
     * @return $this
     */
    public function addTemplateDirectory(string $_templateDirectory)
    {
        $this->templateDirectories = array_unique(array_merge($this->templateDirectories, [$_templateDirectory]));
        return $this;
    }
    
    /**
     * @return string[]
     */
    public function getTemplateDirectories(): array
    {
        if(empty($this->templateDirectories)){
            throw new LogicException("No template directories were configured.");
        }
        
        return $this->templateDirectories;
    }
    
    /**
     * Set the renderer template compile cache directory
     * Since those files are generated they should be put in WP_CONTENT/uploads/x
     *
     * @param string $_cacheDirectory
     * @return $this
     */
    public function setTemplateCacheDirectory(string $_cacheDirectory)
    {
        $this->cacheDirectory = $_cacheDirectory;
        return $this;
    }
    
    public function getTemplateCacheDirectory(): ?string
    {
        return $this->cacheDirectory;
    }
    
    
    /**
     * Adds a controller that will be registered upon initialization
     *
     * @param ControllerInterface[] $_controllers
     * @return $this
     */
    public function addControllers(array $_controllers)
    {
        foreach ($_controllers as $controller){
            if(!$controller instanceof ControllerInterface){
                throw new InvalidArgumentException("Expected controller to be instance of ".ControllerInterface::class.".");
            }
            
            if(!in_array($controller, $this->controllers, true)){
                $this->controllers[] = $controller;
        
                if($this->init){
                    $controller->register();
                }
            }
        }
        
        return $this;
    }
    
    /**
     * @return ControllerInterface[]
     */
    public function getControllers(): array
    {
        return $this->controllers;
    }
    
    
    /**
     * Adds a shortcode that will be registered upon initialization
     *
     * @param Shortcode[] $_shortcodes
     * @return $this
     */
    public function addShortcodes(array $_shortcodes)
    {
        foreach ($_shortcodes as $shortcode){
            if(!$shortcode instanceof Shortcode){
                throw new InvalidArgumentException("Expected shortcode to be instance of ".Shortcode::class.".");
            }
            
            if(!in_array($shortcode, $this->shortcodes, true)){
                $this->shortcodes[] = $shortcode;
                
                if($this->init){
                    $shortcode->register();
                }
            }
        }

        return $this;
    }
    
    /**
     * @return Shortcode[]
     */
    public function getShortCodes(): array
    {
        return $this->shortcodes;
    }
    
    
    /**
     * Configures the renderer once the renderer is created
     * Allows definition of extensions and such
     *
     * Expects a callback with first parameter being the renderer instance
     *
     * @param callable $_configurator
     * @return $this
     */
    public function configureRenderer(callable $_configurator)
    {
        $this->rendererExtensions[] = $_configurator;
        return $this;
    }
    
    public function getRendererConfigurator(): ?callable
    {
        return !empty($this->rendererExtensions) ? function($_renderer){
            foreach ($this->rendererExtensions as $extension){
                call_user_func($extension, $_renderer);
            }
        } : null;
    }
}