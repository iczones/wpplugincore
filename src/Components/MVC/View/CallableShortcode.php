<?php


namespace ICZones\WPCore\Components\MVC\View;


class CallableShortcode extends Shortcode
{
    protected $tag;
    protected $callable;
    protected $defaultParams;
    
    public function __construct(string $_tag, callable $_callable, array $_defaultParams = array())
    {
        $this->tag = $_tag;
        $this->callable = $_callable;
        $this->defaultParams = $_defaultParams;
    }
    
    public function getTag(): string
    {
        return $this->tag;
    }
    
    protected function getDefaultParams(): array
    {
        return $this->defaultParams;
    }
    
    protected function render(array $_params, $_content): string
    {
        return call_user_func($this->callable, $_params, $_content);
    }
}