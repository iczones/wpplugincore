<?php


namespace ICZones\WPCore\Components\MVC\View;


use ICZones\WPCore\Components\MVC\MvcConfig;
use ICZones\WPCore\Components\ServiceLayer\SingletonTrait;
use Twig\Environment;
use Twig\Loader\FilesystemLoader;

class RendererService
{
    use SingletonTrait;
    
    protected $twig;
    
    public function render(string $_template, array $_context = array()): string
    {
        return $this->loadTwig()->render($_template, $_context);
    }
    
    public function renderBlock(string $_template, string $_block, array $_context = array()): string
    {
        return $this->loadTwig()->load($_template)->renderBlock($_block, $_context);
    }
    
    public function getTwig(): Environment
    {
        return $this->loadTwig();
    }
    
    private function loadTwig(): Environment
    {
        $mvcConfig = MvcConfig::getInstance();
        
        if(!$this->twig instanceof Environment){
            $loader = new FilesystemLoader($mvcConfig->getTemplateDirectories());
            
            $options = ['auto_reload' => true];
            if($mvcConfig->getTemplateCacheDirectory()){
                $options['cache'] = $mvcConfig->getTemplateCacheDirectory();
            }
            
            $this->twig = new Environment($loader, $options);
            
            $extension = $mvcConfig->getRendererConfigurator();
            if(is_callable($extension)){
                call_user_func($extension, $this->twig);
            }
        }
        
        return $this->twig;
    }
}