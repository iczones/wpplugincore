<?php


namespace ICZones\WPCore\Components\MVC\View;


use ICZones\WPCore\Bridge\Wordpress;

abstract class Shortcode
{
    // LIFECYCLE
    /**
     * Process anything that needs to be initiated before rendering this shortcode
     * Like setting up services or controller properties
     *
     * @return void
     */
    protected function onInit(){}
    private $initialized = false;
    
    /**
     * Called right before the shortcode is rendered
     *
     * Returning false will prevent the render from happening and
     * will also set the $_canceled params in the after hook to TRUE
     *
     * @param array $_attributes
     * @return boolean
     */
    protected function beforeRender(array $_attributes): bool { return true; }
    
    /**
     * Called right after the shortcode is rendered
     *
     * @param array $_attributes
     * @param bool $_canceled
     * @return void
     */
    protected function afterRender(array $_attributes, bool $_canceled): void {}
    // END OF LIFECYCLE
    
    protected function getDefaultParams(): array
    {
        return [];
    }
    
    abstract public function getTag(): string;
    
    abstract protected function render(array $_params, $_content): string;
    
    public function register()
    {
        Wordpress::addShortcode($this->getTag(), function($_attributes, $_content = null){
            if(!$this->initialized){
                $this->onInit();
                $this->initialized = true;
            }
            
            if(!is_array($_attributes)){
                $_attributes = [];
            }
            $attributes = Wordpress::shortcodeAtts($this->getDefaultParams(), $_attributes, $this->getTag());
            
            $result = '';
            $continue = $this->beforeRender($attributes);
            if($continue){
                $result = $this->render($attributes, $_content);
            }
            $this->afterRender($attributes, !$continue);
            return $result;
        });
    }
}