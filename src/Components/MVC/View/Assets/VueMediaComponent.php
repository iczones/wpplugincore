<?php


namespace ICZones\WPCore\Components\MVC\View\Assets;


use ICZones\WPCore\Bridge\Wordpress;
use ICZones\WPCore\Components\Assets\CoreAssetInterface;
use ICZones\WPCore\Components\Assets\Script;

class VueMediaComponent extends Script implements CoreAssetInterface
{
    const ID = self::BASE_ASSET_ID.'vue_wp_media_component';
    
    public function __construct()
    {
        parent::__construct(self::ID, '@iczcore/js/vue-wp-media-component.js');
    }
    
    public function enqueue()
    {
        Wordpress::enqueueMedia();
        parent::enqueue();
    }
}