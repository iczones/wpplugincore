<?php


namespace ICZones\WPCore\Components\MVC\View\Assets;


use ICZones\WPCore\Bridge\Wordpress;
use ICZones\WPCore\Components\Assets\AssetInterface;
use ICZones\WPCore\Components\Assets\AssetPathHelper;
use ICZones\WPCore\Components\Assets\CoreAssetInterface;
use ICZones\WPCore\Components\Assets\Script;
use ICZones\WPCore\Components\Assets\Stylesheet;

class Vuejs implements AssetInterface, CoreAssetInterface
{
    const ID = self::BASE_ASSET_ID.'vue_js';
    
    /** @var AssetInterface[] */
    protected $assets;
    
    public function __construct(string $_version)
    {
        $vuejsCdn = Wordpress::WP_DEBUG() ? "https://cdn.jsdelivr.net/npm/vue@{$_version}/dist/vue.js" : "https://cdn.jsdelivr.net/npm/vue@{$_version}";
        
        $this->assets = [
            new Stylesheet(self::ID, AssetPathHelper::resolveAsset('@iczcore/css/vue-cloak.css')),
            new Script(self::ID, $vuejsCdn)
        ];
    }
   
    public function getId(): string
    {
        return self::ID;
    }
    
    public function register()
    {
        foreach ($this->assets as $asset){
            $asset->register();
        }
    }
    
    public function enqueue()
    {
        foreach ($this->assets as $asset){
            $asset->enqueue();
        }
    }
}