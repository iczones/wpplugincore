<?php


namespace ICZones\WPCore\Components\MVC\View\Assets;


use ICZones\WPCore\Components\Assets\CoreAssetInterface;
use ICZones\WPCore\Components\Assets\Stylesheet;

class ScopedBootstrap extends Stylesheet implements CoreAssetInterface
{
    const ID = self::BASE_ASSET_ID.'scoped_bootstrap';
    
    public function __construct()
    {
        parent::__construct(self::ID, '@iczcore/css/bootstrap-scope.min.css');
    }
}