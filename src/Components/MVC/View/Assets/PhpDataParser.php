<?php


namespace ICZones\WPCore\Components\MVC\View\Assets;


use ICZones\WPCore\Components\Assets\CoreAssetInterface;
use ICZones\WPCore\Components\Assets\Script;

class PhpDataParser extends Script implements CoreAssetInterface
{
    const ID = self::BASE_ASSET_ID.'php_data_parser';
    
    public function __construct()
    {
        parent::__construct(self::ID, '@iczcore/js/php-data-parser.js');
    }
}