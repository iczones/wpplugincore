<?php


namespace ICZones\WPCore\Components\MVC\View\Assets;


use ICZones\WPCore\Bridge\Wordpress;
use ICZones\WPCore\Components\Assets\AssetInterface;
use ICZones\WPCore\Components\Assets\AssetPathHelper;
use ICZones\WPCore\Components\Assets\CoreAssetInterface;

class AjaxApiService implements AssetInterface, CoreAssetInterface
{
    const ID = self::BASE_ASSET_ID.'ajax_api_service';
    
    public function getId(): string
    {
        return self::ID;
    }
    
    public function register()
    {
        Wordpress::registerScript(self::ID, AssetPathHelper::resolveAsset('@iczcore/js/api-service.js'), ['jquery']);
    }
    
    public function enqueue()
    {
        Wordpress::localizeScript(self::ID, 'wpAjaxUrl', ['url' => Wordpress::admin_url( 'admin-ajax.php' )]);
        Wordpress::enqueueScript(self::ID);
    }
}