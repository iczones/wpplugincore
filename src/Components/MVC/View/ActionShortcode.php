<?php


namespace ICZones\WPCore\Components\MVC\View;


use ICZones\WPCore\Bridge\Wordpress;

class ActionShortcode extends CallableShortcode
{
    public function __construct(string $_tag, string $_action, array $_defaultParams = array())
    {
        parent::__construct($_tag, function() use($_action){
            ob_start();
            Wordpress::doAction($_action, ...func_get_args());
            return ob_get_clean();
        }, $_defaultParams);
    }
}