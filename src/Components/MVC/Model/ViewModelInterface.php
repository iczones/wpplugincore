<?php


namespace ICZones\WPCore\Components\MVC\Model;


use JsonSerializable;

interface ViewModelInterface extends JsonSerializable
{
    public function toArray(): array;
}