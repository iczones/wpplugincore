<?php


namespace ICZones\WPCore\Components\MVC\Model\Common;


trait ActiveTrait
{
    /** @var bool */
    protected $active = true;
    
    
    public function isActive(): bool
    {
        return $this->active;
    }
    
    public function setActive(bool $_active)
    {
        $this->active = $_active;
    }
}