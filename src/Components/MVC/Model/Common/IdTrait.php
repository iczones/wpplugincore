<?php


namespace ICZones\WPCore\Components\MVC\Model\Common;


trait IdTrait
{
    /** @var int|null */
    protected $id;
    
    public function setId(int $_id)
    {
        $this->id = $_id;
    }
    
    public function getId(): ?int
    {
        return $this->id;
    }
}