<?php


namespace ICZones\WPCore\Components\MVC\Model\Common;


use ICZones\WPCore\Components\I18n\Locale;

trait I18nTrait
{
    protected function getCurrentI18nValue(string $_prop, $_default = null)
    {
        $i18nProp = $_prop.ucfirst(Locale::getCurrent());
        if(property_exists($this, $i18nProp)){
            return $this->$i18nProp;
        }
        
        return $_default;
    }
}