<?php


namespace ICZones\WPCore\Components\MVC\Model\Common;


trait SoftDeleteTrait
{
    /** @var bool */
    protected $deleted = false;
    
    
    public function isDeleted(): bool
    {
        return $this->deleted;
    }
    
    public function deleted()
    {
        $this->deleted = true;
    }
}