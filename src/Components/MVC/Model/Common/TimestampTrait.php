<?php


namespace ICZones\WPCore\Components\MVC\Model\Common;


trait TimestampTrait
{
    /** @var \DateTimeInterface */
    protected $createdAt;
    /** @var \DateTimeInterface */
    protected $updatedAt;
    
    
    public function getCreatedAt(): \DateTimeInterface
    {
        return $this->createdAt;
    }
    
    public function created()
    {
        if(!$this->createdAt instanceof \DateTimeImmutable){
            $this->createdAt = new \DateTimeImmutable();
            $this->updated();
        }
    }
    
    public function getUpdatedAt(): \DateTimeInterface
    {
        return $this->updatedAt;
    }
    
    public function updated()
    {
        $this->updatedAt = new \DateTimeImmutable();
    }
}