<?php


namespace ICZones\WPCore\Components\MVC\Model;


use ArrayIterator;
use IteratorAggregate;
use JsonSerializable;

class ModelListView implements JsonSerializable, IteratorAggregate
{
    /** @var ViewModelInterface[] */
    protected $viewModels;
    protected $total;
    
    public function __construct(array $_viewModels)
    {
        $this->viewModels = $_viewModels;
        $this->total = count($_viewModels);
    }
    
    public function setTotal(int $_total): ModelListView
    {
        $this->total = $_total;
        return $this;
    }
    
    public function jsonSerialize()
    {
        return [
            'items' => array_map(function (ViewModelInterface $_viewModel) {
	            return $_viewModel->toArray();
            }, $this->viewModels),
            'total' => $this->total
        ];
    }
    
    /**
     * @return ArrayIterator|ViewModelInterface[]
     */
    public function getIterator()
    {
        return new ArrayIterator($this->viewModels);
    }
}