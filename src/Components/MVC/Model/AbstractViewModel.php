<?php


namespace ICZones\WPCore\Components\MVC\Model;


use InvalidArgumentException;

abstract class AbstractViewModel implements ViewModelInterface
{
    protected $model;
    
    public function __construct($_model)
    {
        $this->model = $_model;
    }
    
    /**
     * @param $_model
     * @return static
     */
    public static function create($_model)
    {
        return new static($_model);
    }
    
    /**
     * @param $_data
     * @param callable|null $_each
     * @return ModelListView
     */
    public static function createList(array $_data, callable $_each = null): ModelListView
    {
        return new ModelListView(static::createArray($_data, $_each));
    }
    
    /**
     * @param array $_data
     * @param callable|null $_each
     * @return static[]
     */
    public static function createArray(array $_data, callable $_each = null): array
    {
        $views = array_values(array_map([static::class, 'create'], $_data));
    
        if(is_callable($_each)){
            foreach ($views as $view){
                call_user_func($_each, $view);
            }
        }
        
        return $views;
    }
    
    public function getModel()
    {
        return $this->model;
    }
    
    public function jsonSerialize()
    {
        return $this->toArray();
    }
    
    protected function invalidModelTypeException(): InvalidArgumentException
    {
        return new InvalidArgumentException(sprintf("Given view model of type %s is not supported in %s", is_object($this->model) ? get_class($this->model) : gettype($this->model), static::class));
    }
}