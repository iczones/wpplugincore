<?php

namespace ICZones\WPCore\Components\ServiceLayer;

trait SingletonTrait
{
    /** @var static */
    private static $instance;

    /**
     * @return static
     */
    public static function getInstance()
    {
        if(!static::$instance instanceof static){
            static::$instance = new static(...func_get_args());
        }
        return static::$instance;
    }
}