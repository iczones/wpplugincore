<?php


namespace ICZones\WPCore\Components\Persistence;


use ICZones\WPCore\Components\I18n\I18nConfig;
use ICZones\WPCore\Components\I18n\Translator\Catalogs\FileCatalog;
use ICZones\WPCore\Components\Menu\MenuConfig;
use ICZones\WPCore\Components\Menu\MenuItem;
use ICZones\WPCore\Components\Menu\Submenu;
use ICZones\WPCore\Components\MVC\MvcConfig;
use ICZones\WPCore\Components\Persistence\Migrations\PhinxController;
use ICZones\WPCore\Components\ServiceLayer\SingletonTrait;
use LogicException;

class PersistenceConfig
{
    use SingletonTrait;
    
    protected $prefix;
    protected $migrationDirectories;
    protected $phinxCliFile;
    
    public function __construct()
    {
        $this->prefix = null;
        $this->migrationDirectories = array();
        $this->phinxCliFile = null;
    }
    
    /**
     * Sets the table prefix for the tables managed by this component
     *
     * @param string $_prefix
     * @return $this
     */
    public function setTablePrefix(string $_prefix)
    {
        $this->prefix = $_prefix;
        return $this;
    }
    
    public function getTablePrefix(): string
    {
        if($this->prefix === null){
            throw new LogicException("Persistence table prefix is not configured.");
        }
        
        return $this->prefix;
    }
    
    /**
     * Adds a migration directory in which phinx should check
     *
     * @param string $_directory
     * @return $this
     */
    public function addMigrationDirectory(string $_directory)
    {
        $this->migrationDirectories[] = $_directory;
        $this->migrationDirectories = array_unique($this->migrationDirectories);
        return $this;
    }
    
    /**
     * @return string
     */
    public function getMigrationDirectories(): array
    {
        return $this->migrationDirectories;
    }
    
    /**
     * Sets the file used by phinx cli for configuration
     *
     * @param string $_file
     * @return $this
     */
    public function setPhinxCliFile(string $_file)
    {
        $this->phinxCliFile = $_file;
        return $this;
    }
    
    public function getPhinxCliFile(): string
    {
        if(!$this->phinxCliFile){
            throw new LogicException("Phinx CLI file is not configured.");
        }
        
        return $this->phinxCliFile;
    }
    
    /**
     * Enables an admin page for managing migrations and adds it to the given submenu
     *
     * @param string|null $_submenuSlug
     * @param callable|null $_menuItemConfig
     * @return $this
     */
    public function addAdminMigration(?string $_submenuSlug = null, ?callable $_menuItemConfig = null)
    {
        I18nConfig::getInstance()
            ->addTranslationCatalog(new FileCatalog('iczcore', __DIR__.'/../../../translations'), false);
        
        MvcConfig::getInstance()
            ->addTemplateDirectory(__DIR__ . '/Migrations/templates')
            ->addControllers([new PhinxController()]);
        
        if($_submenuSlug !== null){
            MenuConfig::getInstance() // Registers the submenu item asynchronously (only once the submenu is created)
                ->getSubmenuAsync($_submenuSlug, function(Submenu $_submenu) use ($_menuItemConfig){
                $item = $_submenu
                    ->addItem('DB Migrations', 'db-migrations')
                    ->setActionHook(PhinxController::PHINX_ADMIN_MIGRATION_ACTION);
        
                if(is_callable($_menuItemConfig)){
                    call_user_func($_menuItemConfig, $item);
                }
            });
        }
        
        return $this;
    }
}