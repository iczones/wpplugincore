<?php


namespace ICZones\WPCore\Components\Persistence\Repository;


use ICZones\WPCore\Components\Persistence\ConnectionInterface;
use ICZones\WPCore\Components\Persistence\Mapper\AutoIdMapperInterface;
use ICZones\WPCore\Components\Persistence\Mapper\MapperInterface;
use ICZones\WPCore\Components\Persistence\PersistenceConfig;
use ICZones\WPCore\Components\Persistence\WordpressDB;
use ICZones\WPCore\Components\ServiceLayer\SingletonTrait;
use Stefmachine\QueryBuilder\Expressions\Expr;
use Stefmachine\QueryBuilder\Expressions\Func;
use Stefmachine\QueryBuilder\QueryBuilder;

class GenericRepository
{
    protected $modelClass;
    protected $mapper;
    protected $table;
    protected $id;
    
    public function __construct(MapperInterface $_mapper, string $_table, string $_id)
    {
        $this->mapper = $_mapper;
        $this->table = $_table;
        $this->id = $_id;
    }
    
    public function getModelClass(): string
    {
        return $this->mapper->getMappedClass();
    }
    
    protected function getTablePrefix(): string
    {
        return PersistenceConfig::getInstance()->getTablePrefix();
    }
    
    protected function getTableFullName(): string
    {
        return $this->getTablePrefix() . $this->table;
    }
    
    protected function getConnection(): ConnectionInterface
    {
        return WordpressDB::getInstance();
    }
    
    /**
     * Returns one result with the ID
     *
     * @param $_id
     * @return object|array|null
     */
    public function find($_id)
    {
        return $this->findOneBy($this->getPrimaryKeyCriteria($_id));
    }
    
    /**
     * Return one result
     *
     * @param array $_criteria
     * @param null $_orderBy
     * @return object|array|null
     */
    public function findOneBy(array $_criteria, $_orderBy = null)
    {
        $result = $this->getConnection()
            ->fetchSingle(
                QueryBuilder::select($this->selectedColumns())
                    ->from($this->getTableFullName())
                    ->where($this->preCriteria($_criteria))
                    ->orderBy($_orderBy)
            );
        if($result !== null){
            $result = $this->mapper->hydrate($result);
        }
        
        return $result;
    }
    
    /**
     * Returns multiple results
     *
     * @param array $_criteria
     * @param null $_orderBy
     * @param int|null $_offset
     * @param int|null $_limit
     * @return array
     */
    public function findBy(array $_criteria, $_orderBy = null, int $_offset = null, int $_limit = null): array
    {
        $results = $this->getConnection()
            ->fetchMany(
                QueryBuilder::select($this->selectedColumns())
                    ->from($this->getTableFullName())
                    ->where($this->preCriteria($_criteria))
                    ->orderBy($_orderBy)
                    ->limit($_limit)
                    ->offset($_offset)
            );
        return array_map(function($_data){
            return $this->mapper->hydrate($_data);
        }, $results);
    }
    
    /**
     * Returns multiple results
     *
     * @return array
     */
    public function findAll(): array
    {
        return $this->findBy([]);
    }
    
    /**
     * Returns the count of results from the query
     *
     * @param array $_criteria
     * @param null $_orderBy
     * @param int|null $_offset
     * @param int|null $_limit
     * @return int
     */
    public function count(array $_criteria, $_orderBy = null, int $_offset = null, int $_limit = null): int
    {
        return $this->getConnection()
                ->fetchScalar(
                    QueryBuilder::select(["count_result" => Func::count(Expr::allColumns())])
                        ->from($this->getTableFullName())
                        ->where($this->preCriteria($_criteria))
                        ->orderBy($_orderBy)
                        ->limit($_limit)
                        ->offset($_offset)
                ) ?? 0;
    }
    
    /**
     * Returns the count of rows of the table
     *
     * @return int
     */
    public function countAll(): int
    {
        return $this->count([]);
    }
    
    /**
     * Updates the model based on the extracted primary key
     *
     * @param $_model
     * @return bool
     */
    public function updateModel($_model): bool
    {
        $pk = $this->id;
        $extractedData = $this->mapper->extract($_model);
        if (!array_key_exists($pk, $extractedData)){
            throw new \RuntimeException("Could not recover primary key value from model.");
        }
    
        $primaryKey = $extractedData[$pk];
        unset($extractedData[$pk]); // We won't put the PK in update set
        
        if($primaryKey === null){
            return false;
        }
        
        return $this->updateBy($extractedData, $this->getPrimaryKeyCriteria($primaryKey));
    }
    
    /**
     * Updates the rows matching criteria with the data values
     * Column key/value pairs are required
     *
     * @param array $_updateData
     * @param array $_criteria
     * @return bool
     */
    public function updateBy(array $_updateData, array $_criteria): bool
    {
        $qb = QueryBuilder::update()
            ->table($this->getTableFullName())
            ->set($this->preUpdate($_updateData))
            ->where($this->preCriteria($_criteria));
        
        return $this->getConnection()->execute($qb);
    }
    
    /**
     * Updates every row of the table with the data values
     * Column key/value pairs are required
     *
     * @param array $_updateData
     * @return bool
     */
    public function updateAll(array $_updateData): bool
    {
        return $this->updateBy($_updateData, []);
    }
    
    public function insertModel($_model): bool
    {
        return $this->insertModelList([$_model]);
    }
    
    public function insertModelList(array $_models): bool
    {
        $inserts = array();
        foreach ($_models as $model){
            $data = $this->mapper->extract($model);
    
            if(!array_key_exists($this->id, $data) || $data[$this->id] === null){
                unset($data[$this->id]);
            }
    
            $inserts[] = $data;
        }
        
        $result = $this->insert($inserts);
        if($result && $this->mapper instanceof AutoIdMapperInterface){
            $lastId = $this->getConnection()->lastInsertId();
            if($lastId !== null){
                $idOffset = count($inserts);
                foreach ($_models as $model){
                    $idOffset--;
                    $this->setId($model, $lastId - $idOffset);
                }
            }
        }
        
        return $result;
    }
    
    /**
     * Inserts one or multiple rows from the dataset
     * Column key/value pairs are required
     *
     * @param array $_insertData
     * @return bool
     */
    public function insert(array $_insertData): bool
    {
        $first = reset($_insertData);
        if(!is_array($first)){
            $_insertData = [$_insertData];
        }
        
        foreach ($_insertData as $index => $data){
            $_insertData[$index] = array_merge($this->getDefaultValues(), $data);
            $_insertData[$index] = $this->preInsert($_insertData[$index]);
        }
        
        return $this->getConnection()
            ->execute(
                QueryBuilder::insert()
                    ->into($this->getTableFullName())
                    ->values($_insertData)
            );
    }
    
    public function deleteModel($_model)
    {
        return $this->deleteModelList([$_model]);
    }
    
    public function deleteModelList(array $_models)
    {
        $pks = array_map(function($_model){
            $data = $this->mapper->extract($_model);
            if(!array_key_exists($this->id, $data)) {
                throw new \RuntimeException("Could not extract primary key from model.");
            }
            
            return $data[$this->id] !== null ? $this->getPrimaryKeyCriteria($data[$this->id]) : null;
        }, $_models);
        // Ignoring null primary keys
        $pks = array_values(array_filter($pks, function($_pk){
            return $_pk !== null;
        }));
        
        if(empty($pks)){
            // We won't erase the whole table if there is nothing to delete!
            return true;
        }
        
        return $this->deleteBy([
            Expr::orX(...$pks)
        ]);
    }
    
    /**
     * Deletes the row with ID
     *
     * @param $_id
     * @return bool
     */
    public function delete($_id): bool
    {
        return $this->deleteBy($this->getPrimaryKeyCriteria($_id));
    }
    
    /**
     * Deletes the rows matching criteria
     *
     * @param array $_criteria
     * @return bool
     */
    public function deleteBy(array $_criteria): bool
    {
        return $this->getConnection()
            ->execute(
                QueryBuilder::delete()
                    ->from($this->getTableFullName())
                    ->where($this->preCriteria($_criteria))
            );
    }
    
    /**
     * Deletes every row of the table
     *
     * @return bool
     */
    public function deleteAll(): bool
    {
        return $this->deleteBy([]);
    }
    
    /**
     * Returns the default values for insert queries
     * @return array
     */
    protected function getDefaultValues(): array
    {
        return array();
    }
    
    protected function selectedColumns(): array
    {
        return [Expr::allColumns()];
    }
    
    protected function setId($_model, $_id)
    {
        if(!$this->mapper instanceof AutoIdMapperInterface){
            throw new \LogicException("Expected mapper to implement ".AutoIdMapperInterface::class);
        }
    
        $this->mapper->hydrateAutoId($_model, [$this->id => $_id]);
    }
    
    private function getPrimaryKeyCriteria($_id)
    {
        return [$this->id => $_id];
    }
    
    /**
     * Method executed every criteria are used in a query
     * Returning the new criteria to use
     *
     * @param array $_criteria
     * @return array
     */
    protected function preCriteria(array $_criteria): array
    {
        return $_criteria;
    }
    
    /**
     * Method executed for every row of data before an update
     * Returning the new values to update
     *
     * @param array $_dataRow
     * @return array
     */
    protected function preUpdate(array $_dataRow): array
    {
        return $_dataRow;
    }
    
    /**
     * Method executed for every row of data before an insert
     * Returning the new values to be inserted
     *
     * @param array $_dataRow
     * @return array
     */
    protected function preInsert(array $_dataRow): array
    {
        return $_dataRow;
    }
}