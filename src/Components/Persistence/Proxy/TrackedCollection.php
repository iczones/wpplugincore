<?php


namespace ICZones\WPCore\Components\Persistence\Proxy;


use ICZones\WPCore\Common\Collection\Collection;
use ICZones\WPCore\Common\Collection\CollectionInterface;

/**
 * Allows tracking changes within a collection
 * Useful when auto saving related entities
 */
class TrackedCollection implements \IteratorAggregate, CollectionInterface
{
    /** @var CollectionInterface */
    protected $collection;
    
    /** @var CollectionInterface */
    protected $removed;

    /** @var CollectionInterface */
    protected $added;
    
    public function __construct(CollectionInterface $_collection)
    {
        $this->collection = $_collection;
        $this->removed = new Collection($this->getType());
        $this->added = new Collection($this->getType());
    }
    
    public static function track(CollectionInterface $_collection): TrackedCollection
    {
        return new TrackedCollection($_collection);
    }
    
    public function getOriginalCollection(): CollectionInterface
    {
        return $this->collection;
    }
    
    public function add($_element)
    {
        $count = count($this->collection);
        $this->collection->add($_element);
        if($count < count($this->collection)){
            $this->added->add($_element);
        }
    }
    
    public function remove($_element)
    {
        $count = count($this->collection);
        $this->collection->remove($_element);
        if($count > count($this->collection)){
            $this->removed->add($_element);
        }
    }
    
    public function toArray(): array
    {
        return $this->collection->toArray();
    }
    
    public function getType(): string
    {
        return $this->collection->getType();
    }
    
    public function count()
    {
        return $this->collection->count();
    }
    
    public function changed(): bool
    {
        return count($this->added) > 0 || count($this->removed) > 0;
    }
    
    public function getAddedElements(): CollectionInterface
    {
        return $this->added;
    }
    
    public function getRemovedElements(): CollectionInterface
    {
        return $this->removed;
    }
    
    public function getIterator()
    {
        return $this->collection;
    }
}