<?php


namespace ICZones\WPCore\Components\Persistence\Proxy;


trait ProxyTrait
{
    private $initialized = false;
    private $callbacks = array();
    private $proxied = null;
    
    /**
     * Subscribe the callable to the collection loading for post processing
     * The first argument received is the collection itself
     *
     * @param callable $_callable
     * @return $this
     */
    public function onLoad(callable $_callable)
    {
        if($this->initialized){
            $this->invokeOnLoad($_callable);
        }
        else{
            $this->callbacks[] = $_callable;
        }
        return $this;
    }
    
    private function invokeOnLoad(callable $_callable)
    {
        call_user_func($_callable, $this);
    }
    
    private function realInitialize()
    {
        if(!$this->initialized){
            $this->initialized = true;
            $this->proxied = $this->initialize();
            foreach ($this->callbacks as $callback){
                $this->invokeOnLoad($callback);
            }
        }
        
        return $this->proxied;
    }
    
    /**
     * Invoke a method of the class after initializing its proxied subject
     *
     * @param string $_method
     * @param array $_params
     * @return mixed
     */
    private function invoke(string $_method, array $_params)
    {
        return call_user_func_array([$this->realInitialize(), $_method], $_params);
    }
    
    /**
     * Returns the proxied subject loaded
     *
     * @return mixed
     */
    abstract protected function initialize();
}