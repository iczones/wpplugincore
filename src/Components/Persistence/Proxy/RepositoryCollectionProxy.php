<?php


namespace ICZones\WPCore\Components\Persistence\Proxy;


use ICZones\WPCore\Common\Collection\LazyInitializerCollection;
use ICZones\WPCore\Components\Persistence\Repository\GenericRepository;

/**
 * Used for lazy loading a collection of entities from a repository
 */
class RepositoryCollectionProxy extends LazyInitializerCollection
{
    /** @var GenericRepository */
    protected $repository;
    protected $criteria;
    protected $orderBy;
    
    public function __construct(GenericRepository $_repository, array $_criteria = array(), $_orderBy = null)
    {
        $this->repository = $_repository;
        $this->criteria = $_criteria;
        $this->orderBy = $_orderBy;
        
        parent::__construct($this->repository->getModelClass(), function(){
            return $this->repository->findBy($this->criteria, $this->orderBy);
        });
    }
    
    public function count()
    {
        if(!$this->initialized){
            if($this->cachedCount === null){
                $this->cachedCount = $this->repository->count($this->criteria, $this->orderBy);
            }
    
            return $this->cachedCount;
        }
    
        $this->initialize();
        return count($this->collection);
    }
}