<?php


namespace ICZones\WPCore\Components\Persistence\Mapper;


interface AutoIdMapperInterface
{
    public function hydrateAutoId($_instance, array $_data);
}