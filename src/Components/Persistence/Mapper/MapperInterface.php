<?php


namespace ICZones\WPCore\Components\Persistence\Mapper;


interface MapperInterface
{
    /**
     * Hydrates an instance of the class from within and returns it
     *
     * @param array $_data
     * @return object
     */
    public function hydrate(array $_data);
    
    /**
     * Reverse hydrate operation
     *
     * @param $_instance
     * @return array
     */
    public function extract($_instance): array;
    
    /**
     * @return string
     */
    public function getMappedClass(): string;
}