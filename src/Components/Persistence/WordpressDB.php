<?php


namespace ICZones\WPCore\Components\Persistence;


use ICZones\WPCore\Bridge\Wordpress;
use ICZones\WPCore\Components\ServiceLayer\SingletonTrait;
use Stefmachine\QueryBuilder\Builder\QueryBuilderInterface;
use Stefmachine\QueryBuilder\QueryInterface;

class WordpressDB implements ConnectionInterface
{
    use SingletonTrait;
    
    /** @var \wpdb */
    protected $connection;
    protected $queryAdapter;
    
    public function __construct()
    {
        $this->connection = Wordpress::wpdb();
        $this->queryAdapter = new WordpressQueryAdapter();
    }
    
    /**
     * @inheritdoc
     */
    public function fetchScalar($_query, array $_params = array()): ?string
    {
        return $this->connection->get_var(
            $this->query($_query, $_params)
        );
    }
    
    /**
     * @inheritdoc
     */
    public function fetchSingle($_query, array $_params = array()): ?array
    {
        return $this->connection->get_row(
            $this->query($_query, $_params),
            Wordpress::WPDB_ARRAY_A()
        ) ?? null;
    }
    
    /**
     * @inheritdoc
     */
    public function fetchMany($_query, array $_params = array()): array
    {
        return $this->connection->get_results(
            $this->query($_query, $_params),
            Wordpress::WPDB_ARRAY_A()
        ) ?? array();
    }
    
    /**
     * @inheritdoc
     */
    public function execute($_query, array $_params = array()): bool
    {
        return $this->connection->query(
            $this->query($_query, $_params)
        ) !== false;
    }
    
    /**
     * Prepares a statement according to wordpress standards
     *
     * @param QueryBuilderInterface|QueryInterface|string $_query
     * @param array $_params
     * @return string
     */
    private function query($_query, array $_params = array())
    {
        if($_query instanceof QueryBuilderInterface){
            $_query = $_query->getQuery($this->queryAdapter);
        }
        
        if($_query instanceof QueryInterface){
            $queryParams = $_query->getParameters();
            $_query = $_query->getSql();
            
            if(!empty($queryParams)){
                foreach ($queryParams as $param => $value){
                    $queryParams[":{$param}"] = $value;
                    unset($queryParams[$param]);
                }
                
                // Reordering the query parameters array in order of apparition in the SQL query
                preg_match_all('/:v\d+/', $_query, $matches);
                if(!empty($matches[0])){
                    $queryParams = array_merge(array_flip($matches[0]), $queryParams);
                }
                
                $replace = array();
                $params = array();
                foreach ($queryParams as $param => $value){
                    if(gettype($value) === "double"){
                        $placeholder = '%f';
                        $params[] = (float)$value;
                    }
                    else if(gettype($value) === "integer"){
                        $placeholder = '%d';
                        $params[] = (int)$value;
                    }
                    else if($value === null){
                        // Wordpress doesn't accept null as a parameter value
                        // We must replace the parameter with plain NULL
                        $placeholder = 'NULL';
                    }
                    else{
                        $params[] = (string)$value;
                        $placeholder = '%s';
                    }
    
                    $replace[$param] = $placeholder;
                }
                
                // Replacing query with parameters
                $_query = strtr($_query, $replace);
                $_params = $params;
            }
        }
        
        if(empty($_query) || !is_string($_query)){
            throw new \RuntimeException("Expected a non-empty string to be given for querying database.");
        }
        
        return $this->connection->prepare($_query, $_params);
    }
    
    public function lastInsertId(): ?int
    {
        return $this->connection->insert_id;
    }
}