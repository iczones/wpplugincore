<?php


namespace ICZones\WPCore\Components\Persistence;


use Stefmachine\QueryBuilder\Builder\QueryBuilderInterface;
use Stefmachine\QueryBuilder\QueryInterface;

interface ConnectionInterface
{
    /**
     * Returns the first value of the first column of the first row as a string or null from the query
     *
     * @param QueryBuilderInterface|QueryInterface|string $_query
     * @param array $_params
     * @return string|null
     */
    public function fetchScalar($_query, array $_params = array()): ?string;
    
    /**
     * Returns the first row of the query as associative array or null
     *
     * @param QueryBuilderInterface|QueryInterface|string $_query
     * @param array $_params
     * @return array|null
     */
    public function fetchSingle($_query, array $_params = array()): ?array;
    
    /**
     * Returns all rows of the query as associative arrays
     *
     * @param QueryBuilderInterface|QueryInterface|string $_query
     * @param array $_params
     * @return array[]
     */
    public function fetchMany($_query, array $_params = array()): array;
    
    /**
     * Returns true on success false on failure
     *
     * @param QueryBuilderInterface|QueryInterface|string $_query
     * @param array $_params
     * @return bool
     */
    public function execute($_query, array $_params = array()): bool;
    
    /**
     * Returns the last inserted id
     *
     * @return int|null
     */
    public function lastInsertId(): ?int;
}