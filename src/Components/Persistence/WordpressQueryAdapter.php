<?php


namespace ICZones\WPCore\Components\Persistence;


use Stefmachine\QueryBuilder\Adapter\MysqlQueryAdapter;

class WordpressQueryAdapter extends MysqlQueryAdapter
{
    public function allowsParametersOptimization(): bool
    {
        return false;
    }
}