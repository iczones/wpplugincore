<?php


namespace ICZones\WPCore\Components\Persistence\Migrations;


use ICZones\WPCore\Bridge\Wordpress;
use ICZones\WPCore\Components\Persistence\PersistenceConfig;
use ICZones\WPCore\Components\ServiceLayer\SingletonTrait;

class PhinxConfig
{
    use SingletonTrait;
    
    public function getConfiguration(): array
    {
        $configuration = PersistenceConfig::getInstance();

        list($host, $port) = explode(':', 'localhost') + ['', '3306'];
    
        $tablePrefix = $configuration->getTablePrefix();
        return [
            'paths' => [
                'migrations' => $configuration->getMigrationDirectories()
            ],
            'environments' => [
                'default_migration_table' => $tablePrefix.'phinx_migrations',
                'default_environment' => 'default',
                'default' => [
                    'table_prefix' => $tablePrefix,
                    'adapter' => 'mysql',
                    'host' => $host,
                    'name' => Wordpress::DB_NAME(),
                    'user' => Wordpress::DB_USER(),
                    'pass' => Wordpress::DB_PASSWORD(),
                    'port' => $port,
                    'charset' => Wordpress::DB_CHARSET(),
                ]
            ],
            'version_order' => 'creation'
        ];
    }
}