<?php


namespace ICZones\WPCore\Components\Persistence\Migrations;


use ICZones\WPCore\Bridge\Wordpress;
use ICZones\WPCore\Components\Assets\AssetBank;
use ICZones\WPCore\Components\I18n\FrontendTranslator;
use ICZones\WPCore\Components\MVC\Controller\ActionHookController;
use ICZones\WPCore\Components\MVC\View\Assets\AjaxApiService;
use ICZones\WPCore\Components\MVC\View\Assets\ScopedBootstrap;
use ICZones\WPCore\Components\MVC\View\Assets\Vuejs;
use ICZones\WPCore\Components\Persistence\PersistenceConfig;
use Phinx\Console\PhinxApplication;
use Phinx\Wrapper\TextWrapper;

class PhinxController extends ActionHookController
{
    const PHINX_ADMIN_MIGRATION_ACTION = 'icz_core_phinx_admin_migration_page';
    
    /** @var PersistenceConfig */
    protected $configuration;
    /** @var AssetBank */
    protected $assetBank;
    
    protected static function getActions(): array
    {
        return array(
            self::PHINX_ADMIN_MIGRATION_ACTION => [
                'method' => 'index',
                'api' => true
            ]
        );
    }
    
    protected function onInit()
    {
        $this->configuration = PersistenceConfig::getInstance();
        $this->assetBank = AssetBank::getInstance();
        
        $this->assetBank
            ->add(new AjaxApiService(), false)
            ->add(new ScopedBootstrap(), false)
            ->add(new Vuejs('2'), false)
            ->add(new FrontendTranslator(), false);
    }
    
    protected function beforeAction(array $_params, string $_action): bool
    {
        return true;
    }
    
    public function index(array $_params)
    {
        if(Wordpress::doingAjax()){
            $this->execute($_params);
        }
        else{
            $this->loadPage();
        }
    }
    
    private function loadPage()
    {
        $this->assetBank
            ->enqueue(AjaxApiService::ID)
            ->enqueue(ScopedBootstrap::ID)
            ->enqueue(Vuejs::ID)
            ->enqueue(FrontendTranslator::ID);
        
        echo $this->render('phinx.migration.html.twig');
    }
    
    private function execute(array $_params)
    {
        try{
            $commands = ['status' => 'getStatus', 'migrate' => 'getMigrate', 'rollback' => 'getRollback'];
            $cmdKey = $_params['command'] ?? 'status';
            if(!in_array($cmdKey, array_keys($commands))){
                Wordpress::sendJsonError(['message' => 'Invalid command'], 400);
                return;
            }
        
            $phinx = new PhinxApplication();
            $wrapper = new TextWrapper($phinx, [
                'configuration' => $this->configuration->getPhinxCliFile(),
                'environment' => 'default'
            ]);
        
            Wordpress::sendJsonSuccess([
                'output' => call_user_func([$wrapper, $commands[$cmdKey]]),
                'code' => $wrapper->getExitCode()
            ]);
        }
        catch(\Throwable $_e){
            $error = 'Fatal error: '. $_e->getMessage();
            Wordpress::sendJsonError(['message' => $error], 500);
        }
    }
}