<?php


namespace ICZones\WPCore\Components\Mail;


use ICZones\WPCore\Bridge\Wordpress;
use ICZones\WPCore\Components\Mail\Message\MessageInterface;
use ICZones\WPCore\Components\ServiceLayer\SingletonTrait;

class Mailer implements MailerInterface
{
    use SingletonTrait;
    
    private const OPTIONS = array(self::OPTION_FROM, self::OPTION_REPLY_TO, self::OPTION_CC, self::OPTION_BCC);
    
    const OPTION_FROM = 'From';
    const OPTION_REPLY_TO = 'Reply-To';
    const OPTION_CC = 'Cc';
    const OPTION_BCC = 'Bcc';
    
    public function sendMail($_to, MessageInterface $_mail, array $_options = array()): bool
    {
        $headers = $_mail->getHeaders();
        
        foreach ($this->getOptions($_options) as $name => $value){
            if(!in_array($name, self::OPTIONS)){
                throw new \InvalidArgumentException("Unknown option {$name}.");
            }
            
            $headers[] = "{$name}: {$value}";
        }
        
        return $this->sendWordpressMail($_to, $_mail->getSubject(), $_mail->getContent(), $headers, $_mail->getAttachments());
    }
    
    private function getOptions(array $_userOptions)
    {
        $config = MailConfig::getInstance();
        
        $options = $_userOptions;
        
        $cc = $options[self::OPTION_CC] ?? '';
        if(!empty($config->getAlwaysCc())){
            $cc .= ($cc ? ',' : '').implode(',', $config->getAlwaysCc());
        }
        if($cc){
            $options[self::OPTION_CC] = $cc;
        }
        
        $bcc = $options[self::OPTION_BCC] ?? '';
        if(!empty($config->getAlwaysBcc())){
            $bcc .= ($bcc ? ',' : '').implode(',', $config->getAlwaysBcc());
        }
        if($bcc){
            $options[self::OPTION_BCC] = $bcc;
        }
        
        if($config->getDefaultFrom() && empty($options[self::OPTION_FROM])){
            $options[self::OPTION_FROM] = $config->getDefaultFrom();
        }
        
        if($config->getDefaultReplyTo() && empty($options[self::OPTION_REPLY_TO])){
            $options[self::OPTION_REPLY_TO] = $config->getDefaultReplyTo();
        }
        
        return $options;
    }
    
    private function sendWordpressMail($_to, $_subject, $_message, $_headers = '', $_attachments = array()): bool
    {
        return Wordpress::mail($_to, $_subject, $_message, $_headers, $_attachments) === true;
    }
    
    public static function buildNamedAddress(string $_name, string $_address)
    {
        return "{$_name} <{$_address}>";
    }
}