<?php


namespace ICZones\WPCore\Components\Mail\Message;


class HtmlMessage extends PlainTextMessage
{
    public function getHeaders(): array
    {
        return array('Content-Type: text/html; charset=UTF-8');
    }
}