<?php


namespace ICZones\WPCore\Components\Mail\Message;


trait MessageAttachmentTrait
{
    protected $attachments = array();
    
    public function getAttachments(): array
    {
        return $this->attachments;
    }
    
    public function addAttachment(string $_attachment)
    {
        $this->attachments[] = $_attachment;
        return $this;
    }
}