<?php


namespace ICZones\WPCore\Components\Mail\Message;


use ICZones\WPCore\Components\MVC\View\RendererService;

class HtmlTemplateMessage implements MessageInterface
{
    use MessageAttachmentTrait;
    
    const BLOCK_SUBJECT = 'subject';
    const BLOCK_CONTENT = 'content';
    
    protected $template;
    protected $parameters;
    
    public function __construct(string $_template, array $_parameters = array())
    {
        $this->template = $_template;
        $this->parameters = $_parameters;
    }
    
    public function getSubject(): string
    {
        return $this->render(self::BLOCK_SUBJECT);
    }
    
    public function getContent(): string
    {
        return $this->render(self::BLOCK_CONTENT);
    }
    
    private function render(string $_block): string
    {
        return RendererService::getInstance()->renderBlock($this->template, $_block, $this->parameters);
    }
    
    public function getHeaders(): array
    {
        return array('Content-Type: text/html; charset=UTF-8');
    }
}