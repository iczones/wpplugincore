<?php


namespace ICZones\WPCore\Components\Mail\Message;


interface MessageInterface
{
    public function getSubject(): string;
    
    public function getContent(): string;
    
    public function getHeaders(): array;
    
    /**
     * @return string[]
     */
    public function getAttachments(): array;
    
    
}