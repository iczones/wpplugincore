<?php


namespace ICZones\WPCore\Components\Mail\Message;


class PlainTextMessage implements MessageInterface
{
    use MessageAttachmentTrait;
    
    protected $subject;
    protected $content;
    
    public function __construct(string $_subject, string $_content)
    {
        $this->subject = $_subject;
        $this->content = $_content;
    }
    
    public function getSubject(): string
    {
        return $this->subject;
    }
    
    public function getContent(): string
    {
        return $this->content;
    }
    
    public function getHeaders(): array
    {
        return array('Content-Type: text/plain; charset=UTF-8');
    }
}