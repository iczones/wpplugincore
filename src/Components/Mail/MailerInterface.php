<?php


namespace ICZones\WPCore\Components\Mail;


use ICZones\WPCore\Components\Mail\Message\MessageInterface;

interface MailerInterface
{
    /**
     * @param string|string[] $_to
     * @param MessageInterface $_mail
     * @param array $_options
     * @return bool
     */
    public function sendMail($_to, MessageInterface $_mail, array $_options = array()): bool;
}