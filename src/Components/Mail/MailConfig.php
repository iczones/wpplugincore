<?php


namespace ICZones\WPCore\Components\Mail;


use ICZones\WPCore\Components\ServiceLayer\SingletonTrait;

class MailConfig
{
    use SingletonTrait;
    
    protected $defaultFrom;
    protected $defaultReplyTo;
    protected $alwaysCc;
    protected $alwaysBcc;
    
    public function __construct()
    {
        $this->defaultFrom = '';
        $this->defaultReplyTo = '';
        $this->alwaysCc = [];
        $this->alwaysBcc = [];
    }
    
    /**
     * Adds these BCC on every email sent with the mailer service
     *
     * @param string[] $_alwaysBcc
     * @return $this
     */
    public function setAlwaysBcc(array $_alwaysBcc)
    {
        $this->alwaysBcc = $_alwaysBcc;
        return $this;
    }
    
    public function getAlwaysBcc(): array
    {
        return $this->alwaysBcc;
    }
    
    /**
     * Adds these CC on every email sent with the mailer service
     *
     * @param string[] $_alwaysCc
     * @return $this
     */
    public function setAlwaysCc(array $_alwaysCc)
    {
        $this->alwaysCc = $_alwaysCc;
        return $this;
    }
    
    public function getAlwaysCc(): array
    {
        return $this->alwaysCc;
    }
    
    /**
     * Sets the default FROM header on every mail sent with the mailer service
     *
     * @param string $_defaultFrom
     * @return $this
     */
    public function setDefaultFrom(string $_defaultFrom)
    {
        $this->defaultFrom = $_defaultFrom;
        return $this;
    }
    
    public function getDefaultFrom(): string
    {
        return $this->defaultFrom;
    }
    
    /**
     * Sets the default REPLY-TO header on every mail sent with the mailer service
     *
     * @param string $_defaultReplyTo
     * @return $this
     */
    public function setDefaultReplyTo(string $_defaultReplyTo)
    {
        $this->defaultReplyTo = $_defaultReplyTo;
        return $this;
    }
    
    public function getDefaultReplyTo(): string
    {
        return $this->defaultReplyTo;
    }
}