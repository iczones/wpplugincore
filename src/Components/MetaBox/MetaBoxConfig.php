<?php

namespace ICZones\WPCore\Components\MetaBox;

use ICZones\WPCore\Bridge\Wordpress;
use ICZones\WPCore\Components\ServiceLayer\SingletonTrait;
use InvalidArgumentException;

class MetaBoxConfig
{
    use SingletonTrait;

    protected array $metaBoxes;
    protected bool $init;

    public function __construct()
    {
        $this->metaBoxes = [];
        $this->init = false;

        Wordpress::addAction('add_meta_boxes', function () {
            foreach ($this->getMetaBoxes() as $metaBox) {
                $metaBox->register();
            }
            $this->init = true;
        });
    }

    public function addMetaBoxes(array $_metaBoxes)
    {
        foreach ($_metaBoxes as $metaBox) {
            if (!$metaBox instanceof MetaBox) {
                throw new InvalidArgumentException("Expected meta box to be an instance of ". MetaBox::class .".");
            }
            if (!in_array($metaBox, $this->metaBoxes, true)) {
                $this->metaBoxes[] = $metaBox;
                if ($this->init) {
                    $metaBox->register();
                }
            }
        }
        return $this;
    }

    /**
     * @return MetaBox[]
     */
    public function getMetaBoxes(): array
    {
        return $this->metaBoxes;
    }
}