<?php

namespace ICZones\WPCore\Components\MetaBox;

abstract class MetaBox
{
    public function register()
    {
        add_meta_box(
            $this->getID(),
            $this->getTitle(),
            [$this, 'render'],
            $this->getSupportedScreens(),
            $this->getContext(),
            $this->getPriority(),
            $this->getCallbackArguments()
        );
        add_action('save_post', [$this, 'save']);
    }

    abstract public static function getID(): string;

    abstract public static function getTitle(): string;

    abstract public static function render(\WP_Post $_post): void;

    abstract public static function save(int $_postId): void;

    public function getSupportedScreens(): ?array
    {
        return null;
    }

    public function getContext(): string
    {
        return MetaBoxContextEnum::getDefaultValue();
    }

    public function getPriority(): string
    {
        return MetaBoxPriorityEnum::getDefaultValue();
    }

    public function getCallbackArguments(): ?array
    {
        return null;
    }
}