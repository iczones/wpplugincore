<?php

namespace ICZones\WPCore\Components\MetaBox;

abstract class MetaBoxPriorityEnum
{
    public const HIGH = 'high';
    public const CORE = 'core';
    public const DEFAULT = 'default';
    public const LOW = 'low';

    public static function getDefaultValue(): string
    {
        return self::DEFAULT;
    }
}