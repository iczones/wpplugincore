<?php

namespace ICZones\WPCore\Components\MetaBox;

abstract class MetaBoxContextEnum
{
    public const NORMAL = 'normal';
    public const SIDE = 'side';
    public const ADVANCED = 'advanced';

    public static function getDefaultValue(): string
    {
        return self::ADVANCED;
    }
}