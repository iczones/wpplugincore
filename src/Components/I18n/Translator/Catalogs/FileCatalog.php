<?php


namespace ICZones\WPCore\Components\I18n\Translator\Catalogs;



class FileCatalog implements TranslationCatalogInterface
{
    /** @var string[] */
    protected $loadedTranslations;
    protected $files;
    protected $loadedLocales;
    protected $name;
    
    public function __construct(string $_id, string $_directory)
    {
        $this->name = $_id;
        $this->loadedLocales = [];
        $this->loadedTranslations = array();
        $this->files = array_map(function($_file) use ($_directory){
            return "{$_directory}/{$_file}";
        }, array_diff(scandir($_directory), ['.', '..']));
    }
    
    private function load(string $_lang)
    {
        if(!in_array($_lang, $this->loadedLocales)){
            foreach ($this->files as $file){
                if(strpos($file,".{$_lang}.") !== false){
                    $this->loadFile($file, $_lang);
                }
            }
            
            $this->loadedLocales[] = $_lang;
        }
    }
    
    private function loadFile(string $_translationFile, string $_lang)
    {
        if(!file_exists($_translationFile)){
            throw new \RuntimeException("File {$_translationFile} does not exists.");
        }
        
        if(strpos($_translationFile, '.php') !== false){
            $content = include($_translationFile);
            if(!is_array($content)){
                throw new \RuntimeException("Invalid translation format.");
            }
        }
        else{
            return; // File can't be loaded, do nothing
        }
        
        $translations = $this->flattenTree($content);
        
        if(!isset($this->loadedTranslations[$_lang])){
            $this->loadedTranslations[$_lang] = array();
        }
        
        $this->loadedTranslations[$_lang] = array_merge($this->loadedTranslations[$_lang], $translations);
    }
    
    private function flattenTree(array $_tree, ?string $_parent = null): array
    {
        $array = [];
        foreach ($_tree as $key => $branch){
            $parent = $_parent ? "{$_parent}.{$key}" : $key;
            if(is_array($branch)){
                $array = array_merge($array, $this->flattenTree($branch, $parent));
            }
            else{
                $array[$parent] = $branch;
            }
        }
        return $array;
    }
    
    public function getName(): string
    {
        return $this->name;
    }
    
    public function get(string $_id, string $_lang): string
    {
        $this->load($_lang);
        return !empty($this->loadedTranslations[$_lang]) && !empty($this->loadedTranslations[$_lang][$_id]) ?
            $this->loadedTranslations[$_lang][$_id] :
            $_id;
    }
    
    public function all(string $_lang): array
    {
        $this->load($_lang);
        return $this->loadedTranslations[$_lang] ?? [];
    }
}