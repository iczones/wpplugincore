<?php


namespace ICZones\WPCore\Components\I18n\Translator\Catalogs;


interface TranslationCatalogInterface
{
    public function getName(): string;
    
    public function get(string $_id, string $_lang): string;
    
    public function all(string $_lang): array;
}