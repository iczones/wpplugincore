<?php


namespace ICZones\WPCore\Components\I18n\Translator;


use ICZones\WPCore\Components\I18n\Locale;
use ICZones\WPCore\Components\I18n\Translator\Catalogs\TranslationCatalogInterface;
use ICZones\WPCore\Components\ServiceLayer\SingletonTrait;

class Translator implements TranslatorInterface
{
    use SingletonTrait;
    
    /** @var string */
    protected $locale;
    /** @var TranslationCatalogInterface[] */
    protected $catalogs;
    /** @var string */
    protected $defaultCatalog;
    
    public function __construct()
    {
        $this->catalogs = array();
    }
    
    public function addCatalog(TranslationCatalogInterface $_catalog, bool $_setDefault = false)
    {
        $this->catalogs[] = $_catalog;
        if($_setDefault || count($this->catalogs) == 1){
            $this->setDefaultCatalog($_catalog->getName());
        }
    }
    
    public function setDefaultCatalog(string $_catalog)
    {
        $this->defaultCatalog = $_catalog;
    }
    
    public function getDefaultCatalog(): TranslationCatalogInterface
    {
        return $this->getCatalog($this->defaultCatalog);
    }
    
    private function getCatalog(?string $_catalog = null): TranslationCatalogInterface
    {
        $_catalog = $_catalog ?? $this->defaultCatalog;
        if($_catalog === null){
            throw new \RuntimeException("No translation catalog have been chosen.");
        }
        
        foreach ($this->catalogs as $catalog){
            if($catalog->getName() == $_catalog){
                return $catalog;
            }
        }
    
        throw new \RuntimeException("No translation catalog with name '{$_catalog}' exists.");
    }
    
    /**
     * @return TranslationCatalogInterface[]
     */
    public function getCatalogs(): array
    {
        return $this->catalogs;
    }
    
    public function trans($_id, array $_parameters = [], $_locale = null, $_catalog = null)
    {
        $_locale = $_locale ?? $this->getLocale();
        
        if(preg_match('/^(\w+)::(.+)$/', $_id, $matches)){
            $_catalog = $matches[1];
            $_id = $matches[2];
        }
        
        $trans = $this->getCatalog($_catalog)->get($_id, $_locale);
        $parameters = array_combine(array_map(function($_param){
            return "{{$_param}}";
        }, array_keys($_parameters)), array_values($_parameters));
        
        return strtr($trans, $parameters);
    }
    
    public function transChoice($_id, $_number, array $_parameters = [], $_locale = null, $_catalog = null)
    {
        throw new \BadMethodCallException(__FUNCTION__." of ".__CLASS__." was not implemented.");
    }
    
    public function setLocale($_locale)
    {
        $this->locale = $_locale;
    }
    
    public function getLocale()
    {
        return $this->locale ?? Locale::getCurrent();
    }
}