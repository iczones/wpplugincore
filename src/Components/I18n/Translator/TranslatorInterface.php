<?php


namespace ICZones\WPCore\Components\I18n\Translator;


interface TranslatorInterface
{
    public function trans($_id, array $_parameters = [], $_locale = null, $_catalog = null);
    
    public function transChoice($_id, $_number, array $_parameters = [], $_locale = null, $_catalog = null);
    
    public function setLocale($_locale);
    
    public function getLocale();
}