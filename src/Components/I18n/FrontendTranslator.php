<?php


namespace ICZones\WPCore\Components\I18n;


use ICZones\WPCore\Bridge\Wordpress;
use ICZones\WPCore\Components\Assets\AssetInterface;
use ICZones\WPCore\Components\Assets\AssetPathHelper;
use ICZones\WPCore\Components\Assets\CoreAssetInterface;
use ICZones\WPCore\Components\I18n\Translator\Translator;

class FrontendTranslator implements AssetInterface, CoreAssetInterface
{
    const ID = self::BASE_ASSET_ID.'js_translator';
    
    public function getId(): string
    {
        return self::ID;
    }
    
    public function register()
    {
        Wordpress::registerScript(self::ID, AssetPathHelper::resolveAsset('@iczcore/js/js-translator.js'));
    }
    
    public function enqueue()
    {
        $translator = Translator::getInstance();
        
        $translations = array();
        foreach (Locale::getAvailableLocales() as $locale){
            $translations[$locale] = $translator->getDefaultCatalog()->all($locale);
            foreach ($translator->getCatalogs() as $catalog){
                foreach($catalog->all($locale) as $key => $translation){
                    $translations[$locale]["{$catalog->getName()}::{$key}"] = $translation;
                }
            }
        }
        Wordpress::localizeScript(self::ID, 'iczcorei18n', [
            'translations' => $translations,
            'locale' => $translator->getLocale()
        ]);
        Wordpress::enqueueScript(self::ID);
    }
}