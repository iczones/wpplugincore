<?php


namespace ICZones\WPCore\Components\I18n;


use ICZones\WPCore\Bridge\Wordpress;
use ICZones\WPCore\Bridge\Wpml;

class Locale
{
    public static function getDefaultLocale()
    {
        return self::getAvailableLocales()[0];
    }
    
    public static function getAvailableLocales(): array
    {
        return I18nConfig::getInstance()->getAvailableLocales();
    }

	public static function getCurrent(): string
	{
	    $wpLocale = Wpml::isActive() ? Wpml::getCurrentLanguage() : Wordpress::getLocale();
		$current =  explode('_', strtolower($wpLocale))[0] ?? self::getAvailableLocales()[0] ?? null;
		if(!in_array($current, self::getAvailableLocales())){
		    @trigger_error('Current locale not found in available locales. Reset to \''.self::getDefaultLocale().'\'.');
		    return self::getDefaultLocale();
        }
		
		return $current;
	}
    
    public static function is(string $_lang): bool
    {
        return self::getCurrent() === $_lang;
	}
}