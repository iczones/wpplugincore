<?php


namespace ICZones\WPCore\Components\I18n;


use ICZones\WPCore\Bridge\Wordpress;
use ICZones\WPCore\Components\I18n\Translator\Catalogs\TranslationCatalogInterface;
use ICZones\WPCore\Components\I18n\Translator\Translator;
use ICZones\WPCore\Components\MVC\MvcConfig;
use ICZones\WPCore\Components\ServiceLayer\SingletonTrait;
use LogicException;
use Twig\Environment;
use Twig\TwigFilter;

class I18nConfig
{
    use SingletonTrait;
    
    protected $translatorEnabled;
    
    protected $availableLocales;
    
    public function __construct()
    {
        $this->translatorEnabled = false;
        $this->availableLocales = array();
    }
    
    /**
     * Sets the available translation locales
     * Consider making an enum to contain them
     *
     * @param array $_availableLocales
     * @return $this
     */
    public function setAvailableLocales(array $_availableLocales)
    {
        $this->availableLocales = $_availableLocales;
        return $this;
    }
    
    /**
     * @return string[]
     */
    public function getAvailableLocales(): array
    {
        if(empty($this->availableLocales)){
            throw new LogicException("Available locales are not configured.");
        }
        
        return $this->availableLocales;
    }
    
    
    /**
     * Sets gettext domain and resource directory from which gettext will get its PO/MO files
     *
     * @param string $_domain
     * @param string $_resourceDirectory
     * @return $this
     */
    public function setGettextResources(string $_domain, string $_resourceDirectory)
    {
        Wordpress::addAction('plugins_loaded', function () use ($_domain, $_resourceDirectory) {
            Wordpress::loadPluginTextDomain(
                $_domain,
                $_resourceDirectory
            );
        });
        
        return $this;
    }
    
    
    /**
     * Adds a translation catalog to the translator and at the same time enables the translator
     * Contrary to gettext resource, a catalog is used within the translator
     *
     * @param TranslationCatalogInterface $_catalog
     * @param bool $_setDefault
     * @return $this
     */
    public function addTranslationCatalog(TranslationCatalogInterface $_catalog, bool $_setDefault = true)
    {
        if(!$this->translatorEnabled){
            MvcConfig::getInstance()
                ->configureRenderer(function(Environment $_twig){
                    $_twig->addFilter(new TwigFilter('trans', [Translator::getInstance(), 'trans']));
                });
        
            $this->translatorEnabled = true;
        }
        
        Wordpress::addAction('plugins_loaded', function () use ($_catalog, $_setDefault) {
            Translator::getInstance()->addCatalog($_catalog, $_setDefault);
        });
        
        return $this;
    }
}