<?php


namespace ICZones\WPCore\Components\Seo;


interface SitemappableInterface
{
    protected const UPDATE_DAILY = 'daily';
    protected const UPDATE_WEEKLY = 'weekly';
    protected const UPDATE_MONTHLY = 'monthly';
    protected const UPDATE_YEARLY = 'yearly';
    
    /**
     * The full url location of the resource
     *
     * @return string
     */
    public function getLocation(): string;
    
    /**
     * The priority of the sitemap entry
     *
     * @return float
     */
    public function getPriority(): float;
    
    /**
     * The ATOM date of last modification
     *
     * @return string
     */
    public function getLastModificationDate(): string;
    
    /**
     * The update frequency expected
     *
     * @return string
     */
    public function getUpdateFrequency(): string;
}