<?php


namespace ICZones\WPCore\Components\Seo;


use ICZones\WPCore\Bridge\Wordpress;
use ICZones\WPCore\Components\ServiceLayer\SingletonTrait;

class SeoConfig
{
    use SingletonTrait;
    
    protected $generators;
    protected $init;
    
    public function __construct()
    {
        $this->generators = array();
        $this->init = false;
        
        Wordpress::addAction('init', function(){
            foreach ($this->getSitemapGenerators() as $generator){
                $generator->register();
            }
            $this->init = true;
        }, 99);
    }
    
    public function addSitemapGenerator(YoastSitemapGenerator $_siteMapGenerator)
    {
        if(!in_array($_siteMapGenerator, $this->generators, true)){
            $this->generators[] = $_siteMapGenerator;
            
            if($this->init){
                $_siteMapGenerator->register();
            }
        }
        return $this;
    }
    
    public function getSitemapGenerators(): array
    {
        return $this->generators;
    }
}