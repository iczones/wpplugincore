<?php

namespace ICZones\WPCore\Components\Seo;

use ICZones\WPCore\Bridge\Wordpress;
use ICZones\WPCore\Bridge\Yoast;
use ICZones\WPCore\Common\Collection\CollectionInterface;
use InvalidArgumentException;
use LogicException;

class YoastSitemapGenerator
{
	/** @var string */
	protected $sitemapName;
	/** @var SitemappableInterface[] */
	protected $sitemappables;

	public function __construct(string $_sitemapName, CollectionInterface $_sitemappables)
	{
	    if(!Yoast::isActive()){
            throw new LogicException('Yoast is required to generate a sitemap.');
        }
	    
	    if(!class_exists($_sitemappables->getType()) || $_sitemappables->getType() !== SitemappableInterface::class
            && !in_array(SitemappableInterface::class, class_implements($_sitemappables->getType()))){
            throw new InvalidArgumentException('Expected collection of '.SitemappableInterface::class.'.');
        }
	    
	    $this->sitemapName = $_sitemapName;
	    $this->sitemappables = $_sitemappables;
	}
    
    public function register()
    {
        // Calling protected method in closures prevents public scoping of class methods while allowing extension
        Wordpress::addFilter('wpseo_sitemap_index', function(){
            return $this->renderIndex();
        });
        $this->registerSitemap();
	}
    
    protected function registerSitemap()
	{
	    Yoast::getSitemaps()->register_sitemap($this->sitemapName, function(){
	        $sitemap = $this->renderSitemap();
	        if(empty($sitemap)){
                Yoast::getSitemaps()->bad_sitemap = true;
            }
	        else{
                Yoast::getSitemaps()->set_sitemap($sitemap);
            }
        });
	}
    
    protected function renderIndex(): string
    {
        $location = Wordpress::siteUrl("{$this->sitemapName}-sitemap.xml");
        return "<sitemap><loc>{$location}</loc></sitemap>";
    }
    
    protected function renderSitemap(): string
    {
        if(count($this->sitemappables) === 0){
            return '';
        }
        
        $sitemap = implode(array_map(function(SitemappableInterface $_sitemappable){
            return Yoast::getSitemaps()->renderer->sitemap_url([
                'loc' => $_sitemappable->getLocation(),
                'pri' => $_sitemappable->getPriority(),
                'mod' => $_sitemappable->getLastModificationDate(),
                'chf' => $_sitemappable->getUpdateFrequency(),
            ]);
        }, $this->sitemappables->toArray()));
        
        $xmlSpecs = implode(' ', [
            'xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"',
            'xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd"',
            'xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"'
        ]);
    
        return "<urlset {$xmlSpecs}>{$sitemap}</urlset>";
	}
}