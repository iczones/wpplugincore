<?php


namespace ICZones\WPCore\Components\Storage\File;


use ICZones\WPCore\Common\Collection\CollectionInterface;

interface FileStorageInterface
{
    /**
     * Stores the content and returns the file name for retrieval
     *
     * @param $_content
     * @return mixed
     */
    public function storeContent($_content, string $_name): string;
    
    /**
     * Returns the content from the file or null if not found
     *
     * @param $_filename
     * @return mixed
     */
    public function getContent($_filename): ?string;
    
    /**
     * Returns a collection of file names
     *
     * @return CollectionInterface
     */
    public function getAll(): CollectionInterface;
    
    /**
     * Removes the file from the storage
     *
     * @param $_filename
     * @return bool
     */
    public function remove($_filename): bool;
    
    /**
     * Returns TRUE if the file was found, FALSE otherwise
     *
     * @param $_filename
     * @return bool
     */
    public function has($_filename): bool;
}