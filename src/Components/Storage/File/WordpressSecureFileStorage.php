<?php


namespace ICZones\WPCore\Components\Storage\File;


use ICZones\WPCore\Bridge\Wordpress;
use ICZones\WPCore\Common\Collection\Collection;
use ICZones\WPCore\Common\Collection\CollectionInterface;
use ICZones\WPCore\Components\ServiceLayer\SingletonTrait;
use ICZones\WPCore\Components\Storage\StorageConfig;

class WordpressSecureFileStorage implements FileStorageInterface
{
    use SingletonTrait;
    
    private const HTACCESS_TEMPLATE = __DIR__ . "/default.htaccess.txt";
    private const HTACCESS_NAME = '.htaccess';
    
    protected $fs;
    
    public function __construct()
    {
        $this->fs = Wordpress::getDirectFileSystem();
    }
    
    private function getDirectory(): string
    {
        return StorageConfig::getInstance()->getSecureFileStorageDirectory();
    }
    
    public function storeContent($_content, string $_name): string
    {
        $filename = $this->generateUniqueFilename($_name);
    
        $this->initializeSecuredDirectory();
        $this->fs->put_contents($this->getFilePath($filename), $_content);
        
        return $filename;
    }
    
    public function getContent($_filename): ?string
    {
        $file = $this->getFilePath($_filename);
        if($this->fs->exists($file)){
            return $this->fs->get_contents($file);
        }
        
        return null;
    }
    
    public function remove($_filename): bool
    {
        $file = $this->getFilePath($_filename);
        if($this->fs->exists($file)){
            $this->fs->delete($file);
        }
    
        return true;
    }
    
    public function has($_filename): bool
    {
        return !empty($_filename) && $this->fs->exists($this->getFilePath($_filename));
    }
    
    public function getAll(): CollectionInterface
    {
        $files = [];
        $directory = $this->getDirectory();
        if($this->fs->exists($directory)){
            $files = array_diff(scandir($directory), array('..', '.', self::HTACCESS_NAME));
        }
        
        return new Collection('string', $files);
    }
    
    public function getFilePath($_filename): string
    {
        $filename = Wordpress::sanitizeFileName($_filename); // Removes possibility of directory traversal
        
        return "{$this->getDirectory()}/{$filename}";
    }
    
    private function generateUniqueFilename(string $_filename): string
    {
        ['filename' => $filename, 'extension' => $ext] = pathinfo($_filename);
        $uniqueId = uniqid();
        $uniqueFilename = "{$filename}.{$uniqueId}.{$ext}";
        
        return Wordpress::sanitizeFileName($uniqueFilename);
    }
    
    private function initializeSecuredDirectory()
    {
        $directory = $this->getDirectory();
        if(!$this->fs->exists($directory)){
            $this->fs->mkdir($directory, 755);
        }
        
        $htaccess = "{$directory}/".self::HTACCESS_NAME;
        if(!$this->fs->exists($htaccess)){
            $this->fs->copy(self::HTACCESS_TEMPLATE, $htaccess);
        }
    }
}