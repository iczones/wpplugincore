<?php


namespace ICZones\WPCore\Components\Storage;


use ICZones\WPCore\Components\ServiceLayer\SingletonTrait;
use LogicException;

class StorageConfig
{
    use SingletonTrait;
    
    protected $securedFileStorageDirectory;
    
    /**
     * Sets the SecureFileStorage directory from which files are privately stored
     *
     * @param string $_directory
     * @return $this
     */
    public function setSecureFileStorageDirectory(string $_directory)
    {
        $this->securedFileStorageDirectory = $_directory;
        return $this;
    }
    
    public function getSecureFileStorageDirectory(): string
    {
        if($this->securedFileStorageDirectory === null){
            throw new LogicException("Secured file storage directory is not configured.");
        }
        
        return $this->securedFileStorageDirectory;
    }
}