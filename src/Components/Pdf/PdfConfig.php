<?php


namespace ICZones\WPCore\Components\Pdf;


use ICZones\WPCore\Components\ServiceLayer\SingletonTrait;
use RuntimeException;

class PdfConfig
{
    use SingletonTrait;
    
    protected $enabled;
    
    public function __construct()
    {
        $this->enabled = false;
    }
    
    /**
     * Enables the use of HtmlTemplatePdf. If Dompdf is not found, an error is thrown.
     *
     * @return $this
     */
    public function enable()
    {
        if(!class_exists('Dompdf\Dompdf')){
            throw new RuntimeException("Dompdf library is required in order to use ".self::class.".");
        }
        
        $this->enabled = true;
        return $this;
    }
    
    public function isEnabled(): bool
    {
        return $this->enabled;
    }
}