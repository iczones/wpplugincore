<?php


namespace ICZones\WPCore\Components\Pdf;


use Dompdf\Dompdf;
use Dompdf\Options;
use ICZones\WPCore\Components\MVC\View\RendererService;
use LogicException;

class HtmlTemplatePdf
{
    protected $template;
    protected $parameters;
    protected $domPdf;
    private $rendered;
    
    public function __construct(string $_template, array $_parameters = array(), ?callable $_configurator = null)
    {
        if(!PdfConfig::getInstance()->isEnabled()){
            throw new LogicException("Pdf component is disabled. Enable it through configurations before using it.");
        }
        
        $this->rendered = false;
        $this->template = $_template;
        $this->parameters = $_parameters;
        $options = new Options();
        if(is_callable($_configurator)){
            call_user_func($_configurator, $options);
        }
        $this->domPdf = new Dompdf($options);
    }
    
    private function render()
    {
        if(!$this->rendered){
            $this->domPdf->loadHtml(
                RendererService::getInstance()->render($this->template, $this->parameters)
            );
            
            $this->domPdf->render();
        }
    }
    
    public function getContent()
    {
        $this->render();
        return $this->domPdf->output();
    }
}