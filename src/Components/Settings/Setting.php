<?php


namespace ICZones\WPCore\Components\Settings;



use ICZones\WPCore\Components\MVC\Model\Common\IdTrait;
use InvalidArgumentException;

class Setting
{
    const TYPE_INTEGER = 'integer';
    const TYPE_FLOAT = 'float';
    const TYPE_BOOLEAN = 'boolean';
    const TYPE_STRING = 'string';
    
    const TYPES = array(
        self::TYPE_INTEGER,
        self::TYPE_FLOAT,
        self::TYPE_BOOLEAN,
        self::TYPE_STRING
    );
    
    use IdTrait;
    
    /** @var string */
    protected $name;
    /** @var mixed */
    protected $value;
    /** @var string */
    protected $type;
    
    public function __construct(string $_name, string $_type)
    {
        $this->name = $_name;
        
        if(!in_array($_type, self::TYPES)){
            throw new InvalidArgumentException("Invalid setting type given '{$_type}'.");
        }
    }
    
    public function getValue()
    {
        return $this->value;
    }
    
    public function setValue($_value)
    {
        $this->value = $_value;
    }
    
    public function getName(): string
    {
        return $this->name;
    }
    
    public function getType(): string
    {
        return $this->type;
    }
}