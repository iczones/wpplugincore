<?php


namespace ICZones\WPCore\Components\Settings;


use ICZones\WPCore\Components\Persistence\Mapper\AutoIdMapperInterface;
use ICZones\WPCore\Components\Persistence\Mapper\MapperInterface;
use ICZones\WPCore\Components\Persistence\Repository\GenericRepository;
use ICZones\WPCore\Components\ServiceLayer\SingletonTrait;

class SettingRepository extends GenericRepository implements MapperInterface, AutoIdMapperInterface
{
    use SingletonTrait;
    
    public function __construct()
    {
        parent::__construct($this, 'setting', 'id');
    }
    
    public function hydrateAutoId($_instance, array $_data)
    {
        if($_instance instanceof Setting){
            $_instance->setId($_data['id']);
        }
    }
    
    public function hydrate(array $_data)
    {
        $setting = new Setting($_data['name'], $_data['type']);
        $setting->setId($_data['id']);
        $setting->setValue($_data['value']);
        
        return $setting;
    }
    
    public function extract($_instance): array
    {
        $extract = array();
        if($_instance instanceof Setting){
            $extract = array(
                'id' => $_instance->getId(),
                'name' => $_instance->getName(),
                'type' => $_instance->getType(),
                'value' => $_instance->getValue()
            );
        }
        
        return $extract;
    }
    
    public function getMappedClass(): string
    {
        return Setting::class;
    }
    
    
}