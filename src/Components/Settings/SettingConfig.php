<?php


namespace ICZones\WPCore\Components\Settings;


use ICZones\WPCore\Components\Persistence\PersistenceConfig;
use ICZones\WPCore\Components\ServiceLayer\SingletonTrait;

class SettingConfig
{
    use SingletonTrait;
    
    protected $enabled;
    
    public function __construct()
    {
        $this->enabled = false;
    }
    
    public function enable()
    {
        PersistenceConfig::getInstance()->addMigrationDirectory(__DIR__.'/migrations');
        $this->enabled = true;
        return $this;
    }
    
    public function isEnabled(): bool
    {
        return $this->enabled;
    }
}