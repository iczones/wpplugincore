<?php


namespace ICZones\WPCore\Components\Settings;


use ICZones\WPCore\Components\Persistence\Proxy\RepositoryCollectionProxy;
use ICZones\WPCore\Components\ServiceLayer\SingletonTrait;
use LogicException;

class Settings
{
    use SingletonTrait;
    
    protected $dbSettings;
    
    public function __construct()
    {
        if(!SettingConfig::getInstance()->isEnabled()){
            throw new LogicException("Setting component is not enabled. Please enabled it in configuration before using it.");
        }
        
        $this->dbSettings = new RepositoryCollectionProxy(SettingRepository::getInstance());
    }
	
    protected function getValue(string $_name)
    {
        $setting = $this->getDbSetting($_name);
        if($setting instanceof Setting){
            return $setting->getValue();
        }
        return null;
    }
    
    protected function getDbSetting(string $_name): ?Setting
    {
        foreach ($this->dbSettings as $setting){
            if($setting->getName() == $_name){
                return $setting;
            }
        }

        return null;
    }
    
    protected function setValue(string $_name, $_value)
    {
        $setting = $this->getDbSetting($_name);
        if($setting instanceof Setting){
            $setting->setValue($_value);
            SettingRepository::getInstance()->updateModel($setting);
        }
    }
}