<?php


namespace ICZones\WPCore\Components\PostTypes;


use ICZones\WPCore\Bridge\Wordpress;
use ICZones\WPCore\Components\I18n\Translator\Translator;

abstract class Taxonomy
{
	/** @var Translator */
	private $translator;

	protected $labelKeys;

	public function __construct()
	{
		$this->translator = Translator::getInstance();
		$this->labelKeys = [
			'name',
			'singular_name',
			'search_items',
			'popular_items',
			'all_items',
			'parent_item',
			'edit_item',
			'view_item',
			'update_item',
			'add_new_item',
			'new_item_name',
			'separate_items_with_commas',
			'add_or_remove_items',
			'choose_from_most_used',
			'not_found',
			'no_terms',
			'filter_by_item'
		];
	}

	abstract public static function getKey(): string;

	abstract public static function getPostTypeKey(): string;

	abstract public static function getSlugBase(): string;

	public function getLabels(): array
	{
		return [];
	}

	public function isPublic(): bool
	{
		return true;
	}

	public function isHierarchical(): bool
	{
		return false;
	}

	public function isSlugHierarchical(): bool
	{
		return false;
	}

	public function rewrite(): bool
    {
        return true;
    }

	public function getArgs(): array
	{
		return [];
	}

	public function register()
	{
		$args = $this->getArgs();
		$args['labels'] = $this->getLabels();
		$args['public'] = $this->isPublic();
		$args['hierarchical'] = $this->isHierarchical();

		if($this->rewrite()) {
            $args['rewrite'] = [];
            $args['rewrite']['slug'] = $this->getSlugBase();
            $args['rewrite']['hierarchical'] = $this->isSlugHierarchical();
        } else {
            $args['rewrite'] = $this->rewrite();
        }

		Wordpress::registerTaxonomy($this->getKey(), $this->getPostTypeKey(), $args);
	}

	protected function getTranslatedLabels(string $_transId): array
	{
		$labels = [];
		foreach ($this->labelKeys as $key) {
			$id = $_transId .'.'. $key;
			$label = $this->translator->trans($id);

			if ($label !== $id) {
				$labels[$key] = $label;
			}
		}

		return $labels;
	}
}