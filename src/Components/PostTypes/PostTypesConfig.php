<?php


namespace ICZones\WPCore\Components\PostTypes;


use ICZones\WPCore\Bridge\Wordpress;
use ICZones\WPCore\Components\ServiceLayer\SingletonTrait;
use InvalidArgumentException;

class PostTypesConfig
{
	use SingletonTrait;

	protected $postTypes;
	protected $taxonomies;
	protected $init;

	public function __construct()
	{
		$this->postTypes = [];
		$this->taxonomies = [];

		$this->init = false;
		Wordpress::addAction('init', function() {
			foreach ($this->getPostTypes() as $postType) {
				$postType->register();
			}

			foreach ($this->getTaxonomies() as $taxonomy) {
				$taxonomy->register();
			}

			$this->init = true;
		});
	}

	/**
	 * Adds post types that will be registered upon initialization
	 *
	 * @param PostType[] $_postTypes
	 * @return $this
	 */
	public function addPostTypes(array $_postTypes)
	{
		foreach ($_postTypes as $postType) {
			if (!$postType instanceof PostType) {
				throw new InvalidArgumentException("Expected post type to be an instance of ". PostType::class .".");
			}

			if (!in_array($postType, $this->postTypes, true)) {
				$this->postTypes[] = $postType;

				if ($this->init) {
					$postType->register();
				}
			}
		}

		return $this;
	}

	public function getPostTypes(): array
	{
		return $this->postTypes;
	}

	/**
	 * Adds taxonomies that will be registered upon initialization
	 *
	 * @param Taxonomy[] $_taxonomies
	 * @return $this
	 */
	public function addTaxonomies(array $_taxonomies)
	{
		foreach ($_taxonomies as $taxonomy) {
			if (!$taxonomy instanceof Taxonomy) {
				throw new InvalidArgumentException("Expected taxonomy to be an instance of ". Taxonomy::class .".");
			}

			if (!in_array($taxonomy, $this->taxonomies, true)) {
				$this->taxonomies[] = $taxonomy;

				if ($this->init) {
					$taxonomy->register();
				}
			}
		}

		return $this;
	}

	public function getTaxonomies(): array
	{
		return $this->taxonomies;
	}
}