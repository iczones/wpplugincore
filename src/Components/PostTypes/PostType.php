<?php


namespace ICZones\WPCore\Components\PostTypes;


use ICZones\WPCore\Bridge\Wordpress;
use ICZones\WPCore\Components\I18n\Translator\Translator;

abstract class PostType
{
	/** @var Translator */
	private $translator;

	protected $labelKeys;

	public function __construct()
	{
		$this->translator = Translator::getInstance();
		$this->labelKeys = [
			'name',
			'singular_name',
			'add_new',
			'add_new_item',
			'edit_item',
			'new_item',
			'view_item',
			'view_items',
			'search_items',
			'not_found',
			'not_found_in_trash',
			'parent_item_colon',
			'all_items',
			'archives',
			'attributes',
			'insert_into_item',
			'uploaded_to_this_item',
			'featured_image',
			'set_featured_image',
			'remove_featured_image',
			'use_featured_image',
			'menu_name',
			'filter_items_list',
			'filter_by_date',
			'items_list_navigation',
			'items_list',
			'item_published',
			'item_published_privately',
			'item_reverted_to_draft',
			'item_scheduled',
			'item_updated',
			'item_link',
			'item_link_description'
		];

		if ($this->isSlugHierarchical()) {
			Wordpress::addFilter('post_type_link', array($this, 'postTypeLink'), 1, 2);
			Wordpress::addAction('init', array($this, 'rewriteRules'));
		}
	}

	abstract public static function getKey(): string;

	abstract public static function getSlugBase(): string;

	public function getLabels(): array
	{
		return [];
	}

	public function getCategory(): ?string
	{
		return null;
	}

	public function isPublic(): bool
	{
		return true;
	}

	public function isHierarchical(): bool
	{
		return false;
	}

	public function isSlugHierarchical(): bool
	{
		return false;
	}

	public function rewrite(): bool
    {
        return true;
    }

	public function hasArchive(): bool
	{
		return false;
	}

	public function getMenuIcon(): string
	{
		return 'dashicons-admin-post';
	}

	public function getArgs(): array
	{
		return [];
	}

	public function register()
	{
		$args = $this->getArgs();
		$args['labels'] = $this->getLabels();
		$args['public'] = $this->isPublic();
		$args['hierarchical'] = $this->isHierarchical();
		$args['menu_icon'] = $this->getMenuIcon();

		if($this->rewrite()) {
            $slug = $this->getSlugBase();
            if ($this->isSlugHierarchical()) {
                $slug .= '/%category%';
            }
            $args['rewrite'] = [];
            $args['rewrite']['slug'] = $slug;
        } else {
		    $args['rewrite'] = $this->rewrite();
        }

		if (!is_null($this->getCategory())) {
			$args['taxonomies'] = [$this->getCategory()];
		}

		if ($this->hasArchive()) {
			$args['has_archive'] = $this->getSlugBase();
		}

		Wordpress::registerPostType($this->getKey(), $args);
	}

	public function postTypeLink(string $_postLink, \WP_Post $_post)
	{
		if (!is_null($this->getCategory())) {
			$terms = get_the_terms($_post, $this->getCategory());
			if (!empty($terms) && !$terms instanceof \WP_Error) {
				return str_replace('%category%', $terms[0]->slug, $_postLink);
			}
		}

		return $_postLink;
	}

	public function rewriteRules()
	{
		add_rewrite_rule(
			'^'. $this->getSlugBase() .'/(.*)/(.*)/?$',
			'index.php?post_type='. $this->getKey() .'&name=$matches[2]',
			'top'
		);
	}

	protected function getTranslatedLabels(string $_transId): array
	{
		$labels = [];
		foreach ($this->labelKeys as $key) {
			$id = $_transId .'.'. $key;
			$label = $this->translator->trans($id);

			if ($label !== $id) {
				$labels[$key] = $label;
			}
		}

		return $labels;
	}
}