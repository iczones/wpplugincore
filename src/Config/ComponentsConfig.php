<?php


namespace ICZones\WPCore\Config;


use ICZones\WPCore\Components\Assets\AssetsConfig;
use ICZones\WPCore\Components\I18n\I18nConfig;
use ICZones\WPCore\Components\Log\LogConfig;
use ICZones\WPCore\Components\Mail\MailConfig;
use ICZones\WPCore\Components\Menu\MenuConfig;
use ICZones\WPCore\Components\MetaBox\MetaBoxConfig;
use ICZones\WPCore\Components\MVC\MvcConfig;
use ICZones\WPCore\Components\Pdf\PdfConfig;
use ICZones\WPCore\Components\Persistence\PersistenceConfig;
use ICZones\WPCore\Components\PostTypes\PostTypesConfig;
use ICZones\WPCore\Components\Seo\SeoConfig;
use ICZones\WPCore\Components\ServiceLayer\SingletonTrait;
use ICZones\WPCore\Components\Settings\SettingConfig;
use ICZones\WPCore\Components\Storage\StorageConfig;

class ComponentsConfig
{
    use SingletonTrait;
    
    public function configureAssets(): AssetsConfig
    {
        return AssetsConfig::getInstance();
    }
    
    public function configureI18n(): I18nConfig
    {
        return I18nConfig::getInstance();
    }
    
    public function configureLog(): LogConfig
    {
        return LogConfig::getInstance();
    }
    
    public function configureMail(): MailConfig
    {
        return MailConfig::getInstance();
    }
    
    public function configureMenu(): MenuConfig
    {
        return MenuConfig::getInstance();
    }
    
    public function configureMvc(): MvcConfig
    {
        return MvcConfig::getInstance();
    }
    
    public function configurePdf(): PdfConfig
    {
        return PdfConfig::getInstance();
    }
    
    public function configurePersistence(): PersistenceConfig
    {
        return PersistenceConfig::getInstance();
    }

	public function configurePostTypes(): PostTypesConfig
	{
		return PostTypesConfig::getInstance();
	}

    public function configureMetaBoxes(): MetaBoxConfig
    {
        return MetaBoxConfig::getInstance();
    }
    
    public function configureSeo(): SeoConfig
    {
        return SeoConfig::getInstance();
    }
    
    public function configureSetting(): SettingConfig
    {
        return SettingConfig::getInstance();
    }
    
    public function configureStorage(): StorageConfig
    {
        return StorageConfig::getInstance();
    }
}